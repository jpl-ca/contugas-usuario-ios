//
//  UserPanelVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/7/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class UserPanelVC: UIViewController, BannerMainDelegate{
    
    @IBOutlet weak var v_recibo:UIView!
    @IBOutlet weak var v_consejo:UIView!
    @IBOutlet weak var v_informacion:UIView!
    @IBOutlet weak var v_contuclub:UIView!
    @IBOutlet weak var v_solicitar_presupuesto:UIView!
    @IBOutlet weak var v_mi_cuenta:UIView!
    
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    let settingAppService = SettingAppService()
    let userProfile = UserProfileService()
    
    var title_bar = ""
    var image_banner = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        setupView()
        showUserInfo()
        settingAppService.checkSettings()
        settingAppService.setMainDelegate(self)
    }
    
    func showUserInfo(){
        userProfile.userFirstName(){ (name) -> () in
            self.title_bar = "\(name.capitalized), Bienvenido"
            self.navigationItem.title = self.title_bar
        }
    }
    
    func showBannerMain(){
        let storyboard = UIStoryboard(name: "BannerContugas", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BannerContugasSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func handleTapRec(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Invoice", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InvoiceSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
        
//        let storyboard = UIStoryboard(name: "BannerContugas", bundle: nil)
//        let controller = storyboard.instantiateViewControllerWithIdentifier("BannerContugasSB") as UIViewController
//        controller.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
//        presentViewController(controller, animated: true, completion: nil)
        
    }
    func handleTapCons(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Consejo", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ConsejoSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
    }
    func handleTapInf(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "InfoPanel", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InfoPanelSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
    }
    
    func handleTapContuclub(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Contuclub", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContuclubSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
    }
    func handleTapConsQR(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "QuotationRequest", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "QuotationRequestSB") as UIViewController
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(controller, animated: true, completion: nil)
    }
    func handleTapRecProfile(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "goto_profile", sender: self)
    }
    
    func setupView(){
        let tapRec = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapRec(_:)))
        v_recibo.addGestureRecognizer(tapRec)
        
        let tapCons = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapCons(_:)))
        v_consejo.addGestureRecognizer(tapCons)
        
        let tapInf = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapInf(_:)))
        v_informacion.addGestureRecognizer(tapInf)
        
        let tapContuclub = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapContuclub(_:)))
        v_contuclub.addGestureRecognizer(tapContuclub)
        
        let tapQR = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapConsQR(_:)))
        v_solicitar_presupuesto.addGestureRecognizer(tapQR)
        
        let tapProfile = UITapGestureRecognizer(target: self, action: #selector(UserPanelVC.handleTapRecProfile(_:)))
        v_mi_cuenta.addGestureRecognizer(tapProfile)
    }
    
    func setupToolbar(){
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Panel de Usuario"
    }
}
