//
//  HistorialPagoVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class HistorialPagoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let consumptionInvoiceService  = ConsumptionInvoiceService()
    let bannerContugasService = BannerContugasService()
    
    @IBOutlet weak var tb_event: UITableView!
    @IBOutlet weak var iv_banner: UIImageView!
    
    var pagos : [HistorialPagoE] = [HistorialPagoE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tb_event.delegate = self
        tb_event.dataSource = self
        setupToolbar()
        bannerContugasService.loadBannerPayment(iv_banner)
        
        consumptionInvoiceService.getPaymentHistory(){
            (historialPago) -> () in
             self.pagos = historialPago
            self.tb_event.reloadData()
        }
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pagos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HistorialPagoCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! HistorialPagoCellTable
        
        cell.setCell(pagos[indexPath.row])
        
        return cell
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Historial de Pagos"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(HistorialPagoVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
