//
//  SugerenciaService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SugerenciaService {
    let apiHelper = ApiHelper()
    
    func enviarSugerencia(_ sugerencia:String, callback: @escaping (Bool)->()) {
        let service = "/api/suggestions"
        let params = ["content": sugerencia]
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
