//
//  SettingAppService.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class SettingAppService{
    let bannerContugasService = BannerContugasService()
    let jmStore = JMStore()
    let downloadService = DownloadService()
    var delegate:BannerMainDelegate? = nil
    let fileHelper = FileHelper()
    
    func checkSettings(){
        if(!jmStore.bannerWasDownloaded()){
            downloadBannerList();
        }else{
            
        }
        let hadSession = jmStore.appHadSession()
        if(!hadSession){
            subscribeGCMTopics()
        }
        if(jmStore.bannerWasDownloaded() && !jmStore.bannerMainSeen()){
            showBannerDialog()
        }
    }
    
    func setMainDelegate(_ _delegate:BannerMainDelegate){
        delegate = _delegate
    }
    
    func showBannerDialog(){
        delay(2.0) { () -> () in
            self.downloadService.downloadImage(self.bannerContugasService.getBannerMain()){
                (image) -> () in
                self.fileHelper.saveImage(image, nameImage: VAR.Banner.MAIN)
                self.jmStore.bannerMainSeen(true)
                self.delegate?.showBannerMain()
            }
        }
    }
    
    func delay(_ seconds: Double, completion:@escaping ()->()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
    
    
    
    func downloadBannerList(){
        bannerContugasService.downloadBanner(){
            (b) -> () in
            if(b){
                self.jmStore.bannerWasDownloaded(true)
                if(!self.jmStore.bannerMainSeen()){
                    self.showBannerDialog()
                }
            }
        }
    }
    
    func subscribeGCMTopics(){
    }
}

protocol BannerMainDelegate: class {
    func showBannerMain()
}
