//
//  ColorHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import UIKit

class ColorHelper{
    func colorPrimary() -> UIColor {
        return UIColor(red: 1/255, green: 87/255, blue: 155/255, alpha: 1)
    }
    
    func colorSecondary() -> UIColor {
        return UIColor(red: 157/255, green: 195/255, blue: 77/255, alpha: 1)
    }
    
    func colorSecondaryDark() -> UIColor {
        return UIColor(red: 109/255, green: 179/255, blue: 81/255, alpha: 1)
    }
    
    func colorGreenButton() -> UIColor {
        return UIColor(red: 78/255, green: 113/255, blue: 38/255, alpha: 1)
    }
    
    func colorBlue() -> UIColor {
        return UIColor(red: 79/255, green: 195/255, blue: 247/255, alpha: 1)
    }
    
    func colorOrange() -> UIColor {
        return UIColor(red: 255/255, green: 138/255, blue: 101/255, alpha: 1)
    }
    
    func colorOrange(_ alpha:CGFloat) -> UIColor {
        return UIColor(red: 255/255, green: 138/255, blue: 101/255, alpha: alpha)
    }
    
    func colorWhite() -> UIColor {
        return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    }
    
    func colorGrayLight() -> UIColor {
        return UIColor(red: 228/255, green: 228/255, blue: 228/255, alpha: 1)
    }
    func colorGrayDark() -> UIColor {
        return UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 1)
    }
    
    
    func colorAzul() -> UIColor {
        return UIColor(red: 0/255, green: 78/255, blue: 146/255, alpha: 1)
    }
    
    func colorCeleste() -> UIColor {
        return UIColor(red: 45/255, green: 158/255, blue: 210/255, alpha: 1)
    }
    
    func colorVerdeOscuro() -> UIColor {
        return UIColor(red: 95/255, green: 188/255, blue: 98/255, alpha: 1)
    }
    
    func colorVerdeClaro() -> UIColor {
        return UIColor(red: 149/255, green: 202/255, blue: 93/255, alpha: 1)
    }
    
    func colorChincha() -> UIColor {
        return UIColor(red: 250/255, green: 128/255, blue: 19/255, alpha: 1)
    }
    
    func colorIca() -> UIColor {
        return UIColor(red: 68/255, green: 32/255, blue: 1/255, alpha: 1)
    }
    
    func colorNasca() -> UIColor {
        return UIColor(red: 12/255, green: 148/255, blue: 142/255, alpha: 1)
    }
    
    func colorPisco() -> UIColor {
        return UIColor(red: 46/255, green: 76/255, blue: 148/255, alpha: 1)
    }
}
