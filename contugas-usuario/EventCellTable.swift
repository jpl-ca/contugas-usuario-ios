//
//  EventCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/13/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class EventCellTable: UITableViewCell {

    @IBOutlet weak var iv_event: UIImageView!
    @IBOutlet weak var txt_title: UILabel!
    @IBOutlet weak var txt_attend: UILabel!
    
    let downloadService = DownloadService()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ event:EventE){
        txt_title.text = event.title
        
        downloadService.downloadImage(event.image_url){
            (image) -> () in
            self.iv_event.image = image
        }
        
        txt_attend.text = "🤔"
        if event.attending {
            txt_attend.text = "😊"
        }
    }
}
