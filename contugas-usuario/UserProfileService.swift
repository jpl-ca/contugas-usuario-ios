//
//  UserProfileService.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/26/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class UserProfileService {
    let userStore = UserStore()
    let apiHelper = ApiHelper()
    
    func userProfileInfo(_ callback: (UserE)->()) {
        userStore.userProfileInfo(){ (user) -> () in
            callback(user)
        }
    }
    
    func userFirstName(_ callback: (String)->()){
        userStore.userProfileInfo(){ (user) -> () in
            let user_name = user.name
            let first_name = user_name.characters.split{$0 == " "}.map(String.init)
            let name = first_name[0]
            callback(name)
        }
    }
    
    func LogoutUser(_ email:String,password:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/logout"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.userStore.storeUserData(data)
                callback(true)
            }
        }
    }
    
    func changePassword(_ last_password:String, new_password:String, callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me"
        let params = ["last_password": last_password,"new_password": new_password]
        apiHelper.makePutRequest(service,params:params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.userStore.storeUserData(data)
                callback(true)
            }
        }
    }
}
