//
//  ConsumptionInvoiceE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/21/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ConsumptionInvoiceE : Object{
    dynamic var period:String = ""
    dynamic var rate_category:String = ""
    
    dynamic var volume_invoice:String = ""
    dynamic var total_invoice:String = ""
    dynamic var last_debt:String = ""
    dynamic var current_debt:String = ""
    
    dynamic var month:String = ""
    dynamic var month_name:String = ""
    
    dynamic var year:String = ""
    dynamic var expiration_date:String = ""
    
    func setData(_ period:String,rate_category:String,volume_invoice:String,total_invoice:String,last_debt:String,current_debt:String,  month:String,month_name:String,year:String,expiration_date:String){
        self.period = period
        self.rate_category = rate_category
        self.volume_invoice = volume_invoice
        self.total_invoice = total_invoice
        self.last_debt = last_debt
        self.current_debt = current_debt
        self.month = month
        self.month_name = month_name
        self.year = year
        self.expiration_date = expiration_date
    }
}
