//
//  BannerStore.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class BannerStore{
    let realm = try! Realm()
    
    func storeBanners(_ data:BannerE) {
        try! realm.write {
            realm.delete(realm.objects(BannerE))
        }
        try! realm.write {
            realm.add(data)
        }
    }
    
    func getBanners() -> (BannerE) {
        let banners = realm.objects(BannerE).first!
        return banners
    }
}
