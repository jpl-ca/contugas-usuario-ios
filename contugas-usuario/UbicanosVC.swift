//
//  UbicanosVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/11/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit
import GoogleMaps

enum PLACE_TYPE{
    case markerCenterOfAttention
    case markerPointOfAttention
    case markerCajaIca
    case none
}

class UbicanosVC: UIViewController,SearchPlaceDelegate,GMSMapViewDelegate {
    
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let ubicanosService = UbicanosService()
    var placesList = [PaymentAttentionPlaceE]()
    
    var markerCenterOfAttention = [GMSMarker]()
    var markerPointOfAttention = [GMSMarker]()
    var markerCajaIca = [GMSMarker]()
    
    @IBOutlet weak var viewMap: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        setupMap()
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Ubícanos"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(UbicanosVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
        
        let myBtnSearch: UIButton = UIButton()
        myBtnSearch.setImage(UIImage(named: "find_place-50"), for: UIControlState())
        myBtnSearch.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnSearch.addTarget(self, action: #selector(UbicanosVC.goListSearch(_:)), for: .touchUpInside)
        
        let rightBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnSearch)
        self.navigationItem.setRightBarButtonItems([rightBackButtonItem], animated: true)
    }
    
    func goListSearch(_ sender:UIButton){
        self.performSegue(withIdentifier: "goto_list_place", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController2 = segue.destination as? BusquedaUbicanosVC {
            viewController2.delegate = self
            viewController2.placeList = self.placesList
        }
    }
    
    func placeSelected(_ place: PlaceE, placeType: PLACE_TYPE,idxArea:Int,idxPlaceInArea:Int){
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.lat, longitude: place.lng, zoom: 10.0)
        viewMap.camera = camera
        
        switch placeType {
        case .markerCenterOfAttention:
            viewMap.selectedMarker = markerCenterOfAttention[idxPlaceInArea]
            break
        case .markerPointOfAttention:
            viewMap.selectedMarker = markerPointOfAttention[idxPlaceInArea]
            break
        case .markerCajaIca:
            var i = 0
            var sumRows = 0
            for places in placesList{
                if(i>1&&i<idxArea){
                    sumRows = sumRows + places.places.count
                }
                i = i + 1
            }
            viewMap.selectedMarker = markerCajaIca[sumRows + idxPlaceInArea]
            break
        default:break
        }
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupMap(){
        var me = ubicanosService.myLocation()
        
        if(me.0==0){
            me.0 = -12.054429839262000
            me.1 = -77.024803161621000
        }
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: me.0, longitude: me.1, zoom: 10.0)
        viewMap.camera = camera
        viewMap.delegate = self
        
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.settings.zoomGestures = true
        
        loadPlaces()
        
    }
    
    func loadPlaces(){
        ubicanosService.getAllPlaces(){
            (places) -> () in
            self.placesList = places
            for listPlace in places {
                for place in listPlace.places {
                    self.addMarker(listPlace.place_type,place: place)
                }
            }
        }
    }
    
    func addMarker(_ place_type:PLACE_TYPE,place:PlaceE){
        let position = CLLocationCoordinate2DMake(place.lat, place.lng)
        let marker = GMSMarker(position: position)
        
        marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0)
        marker.userData = place
        
        marker.map = viewMap
        
        switch place_type {
        case PLACE_TYPE.markerCenterOfAttention:
            markerCenterOfAttention.append(marker)
            
        case PLACE_TYPE.markerPointOfAttention:
            markerPointOfAttention.append(marker)
            
        case PLACE_TYPE.markerCajaIca:
            markerCajaIca.append(marker)
            
        default: break
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let infoWindow = Bundle.main.loadNibNamed("CustomInfoWindowUbicanos", owner: self, options: nil)?.first as! CustomInfoWindowUbicanos
        var text = (marker.userData as! PlaceE).name
        infoWindow.addLabel(text,align: NSTextAlignment.center,title:true)
        text = (marker.userData as! PlaceE).address
        infoWindow.addLabel(text,align: NSTextAlignment.center,title:false)
        text = (marker.userData as! PlaceE).phone
        if(text != "-"){
            infoWindow.addLabel(text,align: NSTextAlignment.center,title:false)
        }
        infoWindow.addLabel("Hora de Atención:",align: NSTextAlignment.left,title:true)
        text = (marker.userData as! PlaceE).attention_hour
        infoWindow.addLabel(text,align: NSTextAlignment.left,title:false)
        
        return infoWindow
    }
    
}
