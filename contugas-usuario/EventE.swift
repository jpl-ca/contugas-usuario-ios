//
//  EventE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/27/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class EventE{
    dynamic var id:String = ""
    dynamic var event_id:String = ""
    dynamic var title:String = ""
    dynamic var content:String = ""
    dynamic var constraints:String = ""
    dynamic var date:String = ""
    dynamic var image_path:String = ""
    dynamic var type:String = ""
    dynamic var image_url:String = ""
    dynamic var attending:Bool = false
}