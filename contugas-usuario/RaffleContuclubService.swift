//
//  RaffleContuclubService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/27/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class RaffleContuclubService {
    let apiHelper = ApiHelper()
    var raffleList:[RaffleE] = []
    
    func getRaffles(_ callback: @escaping ([RaffleE])->()) {
        let service = "/api/user/auth/me/events?type=raffle"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(self.raffleList)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.parseEvents(data!)
                callback(self.raffleList)
            }
        }
    }
    
    func parseEvents(_ data:JSON){
        let eventArray = data["data"].arrayValue
        self.raffleList = [RaffleE]()
        
        for val in eventArray {
            let raffle = RaffleE()
            raffle.raffle_id = String(val["id"].int!)
            if(val["constraints"].null == nil){
                raffle.constraints = val["constraints"].string!
            }
            if(val["content"].null == nil){
                raffle.content = val["content"].string!
            }
            raffle.image_url = val["image_url"].string!
            raffle.title = val["title"].string!
            raffle.date = val["date"].string!
            
            raffleList.append(raffle)
        }
    }
}
