//
//  CouponsContuclubService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/27/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class CouponsContuclubService {
    let apiHelper = ApiHelper()
    var couponList:[CouponE] = []
    
    func loadCoupons(_ callback: @escaping ([CouponE])->()) {
        let service = "/api/user/auth/me/coupons?fields=company"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(self.couponList)
            }else{
//                print("JSONN: \(data)")
//                print("D:****************************")
                self.parseCoupons(data!)
                callback(self.couponList)
            }
        }
    }
    
    func parseCoupons(_ data:JSON){
        let couponArray = data["data"].arrayValue
        self.couponList = [CouponE]()
        
        for val in couponArray {
            let coupon = CouponE()
            coupon.id = val["id"].string!
            if(val["description"].null == nil){
                coupon.description = val["description"].string!
            }
            coupon.coupon_code = val["coupon_code"].string!
            coupon.img_url = val["img_url"].string!
            coupon.title = val["title"].string!
            coupon.active = val["active"].bool!
            
            if(val["company"].isExists()){
                coupon.company = parseCompany(val["company"])
            }else{
                print("No hay company...")
            }
            couponList.append(coupon)
        }
    }
    
    func parseCompany(_ companyJson:JSON)->(CompanyE){
        let company = CompanyE()
        company.address = companyJson["address"].string!
        if(companyJson["description"].null == nil){
            company.description = companyJson["description"].string!
        }else{
            company.description = ""
        }
        
        company.name = companyJson["name"].string!
        if(companyJson["ruc"].null == nil){
            company.ruc = companyJson["ruc"].string!
        }else{
            company.ruc = ""
        }
        
        let placesArray = companyJson["company_places"].arrayValue
        
        for place in placesArray {
            company.lat = Double(place["lat"].string!)!
            company.lng = Double(place["lng"].string!)!
        }
        return company
    }
    
    
    func loadCouponsByProvince(_ province_id:String,callback: @escaping ([CouponE])->()) {
        let service = "/api/user/auth/me/coupons?fields=company&province_id="+province_id
        print("->>\(service)")
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(self.couponList)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.parseCoupons(data!)
                callback(self.couponList)
            }
        }
    }
}
