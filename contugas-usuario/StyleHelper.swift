//
//  StyleHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import UIKit

class StyleHelper{
    let colorHelper = ColorHelper()
    func setBackgroundPrimary(_ btn:UIButton){
        btn.backgroundColor = colorHelper.colorPrimary()
    }
    func setBorderPrimary(_ view:UIView){
        view.layer.borderColor = colorHelper.colorPrimary().cgColor
        view.layer.borderWidth = 1.0;
        view.layer.cornerRadius = 5.0;
    }
    func setBorder(_ view:UIView){
        view.layer.borderColor = colorHelper.colorGrayDark().cgColor
        view.layer.borderWidth = 1.0;
        view.layer.cornerRadius = 5.0;
    }
    
    func setNavigationBarStyle(_ navigationController:UINavigationController){
        navigationController.navigationBar.barTintColor = colorHelper.colorPrimary()
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : colorHelper.colorWhite()]
    }
}
