//
//  SorteoDetailVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class SorteoDetailVC: UIViewController {
    var sorteo = RaffleE()
    let downloadService = DownloadService()
    
    @IBOutlet weak var imgSorteo: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        
        downloadService.downloadImage((sorteo.image_url)){
            (image) -> () in
            self.imgSorteo.image = image
        }
        
        txtTitle.text = sorteo.title
        txtFecha.text = sorteo.date
        txtDescription.text = "\(sorteo.content) \n \(sorteo.constraints)"
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupToolbar(){
        styleHelper.setNavigationBarStyle(self.navigationController!)
        
        navigationItem.title = "Sorteo"
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(SorteoDetailVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
