//
//  LogoutService.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/26/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class LogoutService {
    let apiHelper = ApiHelper()
    let jmStore = JMStore()
    let userStore = UserStore()
    
    func logout(_ callback: @escaping (Bool)->()) {
        jmStore.appCloseSession()
        let service = "/api/user/auth/logout"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                callback(false)
            }else{
//                print("JSONN: \(data)")
                self.deleteUserData()
                callback(true)
            }
        }
    }
    
    fileprivate func deleteUserData() {
        userStore.removeUserData()
    }
}
