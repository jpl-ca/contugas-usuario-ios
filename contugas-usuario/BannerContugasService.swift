//
//  BannerContugasService.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class BannerContugasService {
    let apiHelper = ApiHelper()
    var banner = BannerE()
    let bannerStore = BannerStore()
    let fileHelper = FileHelper()
    let downloadService = DownloadService()
    
    init(){
        
    }
    
    func downloadBanner(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/app-components"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {
                print(error)
                print("E:****************************")
                callback(false)
            }else{
//                print("JSONN: \(data)")
//                print("D:****************************")
                self.parseBanners(data)
                callback(true)
            }
        }
    }
    
    func parseBanners(_ data:JSON?){
        let banners = data!["data"]
        banner.MAIN = banners["MAIN"].string!
        banner.PAYMENT = banners["PAYMENT"].string!
        banner.FINANCING = banners["FINANCING"].string!
        banner.HISTORY = banners["HISTORY"].string!
        banner.INVOICE = banners["INVOICE"].string!
        bannerStore.storeBanners(banner)
    }
    
    func loadBannerMain(_ iv_banner_contugas:UIImageView){
        iv_banner_contugas.image = fileHelper.loadImageFromPath(VAR.Banner.MAIN)
    }
    func loadBannerInvoice(_ iv_banner_contugas:UIImageView){
        print("\(banner.INVOICE) - \(banner.MAIN)")
        loadBannerContugas()
        downloadService.downloadImage(banner.INVOICE){
            (image) -> () in
            iv_banner_contugas.image = image
        }
    }
    func loadBannerPayment(_ iv_banner_contugas:UIImageView){
        loadBannerContugas()
        downloadService.downloadImage(banner.PAYMENT){
            (image) -> () in
            iv_banner_contugas.image = image
        }
    }
    func loadBannerFinancing(_ iv_banner_contugas:UIImageView){
        loadBannerContugas()
        downloadService.downloadImage(banner.FINANCING){
            (image) -> () in
            iv_banner_contugas.image = image
        }
    }
    func loadBannerHistory(_ iv_banner_contugas:UIImageView){
        loadBannerContugas()
        downloadService.downloadImage(banner.HISTORY){
            (image) -> () in
            iv_banner_contugas.image = image
        }
    }
    
    func loadBannerContugas() {
        banner = bannerStore.getBanners()
    }
    
    func getBannerMain() -> (String) {
        return banner.MAIN
    }
    
    func getBannerPayment() -> (String) {
        return banner.PAYMENT
    }
    
    func getBannerFinancing() -> (String) {
        return banner.FINANCING
    }
    
    func getBannerHistory() -> (String) {
        return banner.HISTORY
    }
    
    func getBannerInvoice() -> (String) {
        return banner.INVOICE
    }
}
