//
//  LoginService.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class LoginService {
    let apiHelper = ApiHelper()
    let userStore = UserStore()
    
    func LoginUser(_ email:String,password:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/login"
        let params = ["email": email,"password": password,"remember": "1"]
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.userStore.storeUserData(data)
                callback(true)
            }
        }
    }
}
