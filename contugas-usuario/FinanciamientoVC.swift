//
//  FinanciamientoVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class FinanciamientoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let consumptionInvoiceService = ConsumptionInvoiceService()
    let bannerContugasService = BannerContugasService()
    
    @IBOutlet weak var tb_financiamiento: UITableView!
    @IBOutlet weak var iv_banner: UIImageView!
    
    var dataF : [FinancingE] = [FinancingE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tb_financiamiento.delegate = self
        tb_financiamiento.dataSource = self
        
        consumptionInvoiceService.getFinancing(){
            (financiamiento) -> () in
            self.dataF = financiamiento
            self.tb_financiamiento.reloadData()
        }
                                                                                                                       
        setupToolbar()
        bannerContugasService.loadBannerFinancing(iv_banner)
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataF.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FinanciamientoCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! FinanciamientoCellTable
        
        cell.layer.borderColor = colorHelper.colorGrayLight().cgColor
        cell.layer.borderWidth = 2.0
        cell.layer.shadowOpacity = 0.5
        
        cell.setCell(dataF[indexPath.row])
        
        return cell
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        navigationItem.title = "Financiamiento"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(FinanciamientoVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
