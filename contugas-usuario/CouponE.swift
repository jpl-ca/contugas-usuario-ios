//
//  CouponE.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/9/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift

class CouponE{
    dynamic var id:String = ""
    dynamic var title:String = ""
    dynamic var description:String = ""
    dynamic var coupon_code:String = ""
    dynamic var img_url:String = ""
    dynamic var active:Bool = false
    var company = CompanyE()
}