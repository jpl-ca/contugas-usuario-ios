//
//  RegisterService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class RegisterAccountService {
    let apiHelper = ApiHelper()
    let userStore = UserStore()
    
    func RegisterUser(_ customer_number:String,identification_document:String,email:String,password:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/create"
        let params = ["customer_number": customer_number,"identification_document": identification_document,"email": email,"password":password]
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func ValidateUser(_ customer_number:String,identification_document:String,callback: @escaping (Bool)->()) {
        
        let service = "/api/customer/validate"
        let params = ["customer_number": customer_number,"identification_document": identification_document]
        
        print(params)
        
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
