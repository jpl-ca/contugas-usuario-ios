//
//  ListPlaceCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ListPlaceCellTable: UITableViewCell, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var txt_titulo: UILabel!
    @IBOutlet weak var tb_place: UITableView!
    weak var delegate:SelectPlaceDelegate? = nil
    var placeList: [PlaceE] = [PlaceE]()
    var placeType: PLACE_TYPE = PLACE_TYPE.none
    var idxArea = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    func setDelegate(_ _delegate:SelectPlaceDelegate){
        delegate = _delegate
    }
    
    func setIndex(_ idx:Int){
        self.idxArea = idx
    }
    
    func setCell(_ paymentAttentionPlace:PaymentAttentionPlaceE){
        txt_titulo.text = paymentAttentionPlace.name
        placeList = paymentAttentionPlace.places
        placeType = paymentAttentionPlace.place_type
        tb_place.delegate = self
        tb_place.dataSource = self
        tb_place.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let idxPlaceInArea = indexPath.row
        
        let placeSelected = placeList[idxPlaceInArea]
        delegate?.placeSelected(placeSelected, placeType: placeType, idxArea:idxArea,idxPlaceInArea: idxPlaceInArea)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PlaceCellTable = tableView.dequeueReusableCell(withIdentifier: "CellPlace") as! PlaceCellTable
        cell.setCell(placeList[indexPath.row])
        return cell
    }
}

protocol SelectPlaceDelegate: class {
    func placeSelected(_ place: PlaceE,placeType: PLACE_TYPE, idxArea:Int, idxPlaceInArea:Int)
}
