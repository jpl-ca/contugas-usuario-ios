//
//  MenuButtonView.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class MenuButtonView{
    let colorHelper = ColorHelper()
    /**
        MenuButton No es utilizado oficialmente
    */
    func addMenuOptionZone(_ view:UIView, pos:Int, name:String, image:String, color:UIColor, buttonAction:Selector){
        let h = 45
        let w = 45
        let space_button_map = 150
        let paddingRight = CGFloat(55)
        let paddingBottom = CGFloat(space_button_map + ( ( h + 7 ) * pos ) )
        
        let X_Pos = view.frame.size.width - paddingRight
        let Y_Pos = view.frame.size.height - paddingBottom
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: X_Pos, y: Y_Pos, width: CGFloat(w), height: CGFloat(h))
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.backgroundColor = color
        button.setImage(UIImage(named:image), for: UIControlState())
        button.imageEdgeInsets = UIEdgeInsetsMake(35,35,35,35)
        button.addTarget(self, action: buttonAction, for: .touchUpInside)
        view.addSubview(button)
        addLabel(view, name:name, x: X_Pos - CGFloat( w + 27), y: Y_Pos + 12.5)
    }
    
    func addLabel(_ view:UIView, name:String,x:CGFloat,y:CGFloat){
        let label = UILabel(frame: CGRect(x: x, y: y, width: 70, height: 21))
        label.textAlignment = NSTextAlignment.center
        label.text = name
        
        label.backgroundColor = colorHelper.colorGrayDark()
        label.textColor = colorHelper.colorWhite()
        
        label.font = UIFont(name: label.font.fontName, size: 13)
        
        view.addSubview(label)
    }
}
