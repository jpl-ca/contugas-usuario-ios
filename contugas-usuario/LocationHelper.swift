//
//  LocationHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/10/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import CoreLocation

class LocationHelper: NSObject, CLLocationManagerDelegate{
    var location : CLLocation!
    let locManager = CLLocationManager()
    var LatitudeGPS = NSString()
    var LongitudeGPS = NSString()
    
    func configLocationManager(){
        self.locManager.delegate = self
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locManager.requestWhenInUseAuthorization()
        self.locManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations : [CLLocation]){
        manager.stopUpdatingLocation()
        self.location = locations[0]
        LatitudeGPS = String(format: "%.6f", location.coordinate.latitude) as NSString
        LongitudeGPS = String(format: "%.6f", location.coordinate.longitude) as NSString
        print("Esta en: \(LatitudeGPS),\(LongitudeGPS)")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: " + error.localizedDescription)
//        self.locManager.requestAlwaysAuthorization()
    }
    
    
    func getLocation()->(String,String){
        if self.location != nil {
            return (String(LatitudeGPS),String(LongitudeGPS))
        } else {
            print("locationManager.location is nil")
            return ("0","0")
        }
    }
}
