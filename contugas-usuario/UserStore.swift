//
//  UserStore.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/4/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class UserStore {
    let realm = try! Realm()
    
    func storeUserData(_ data:JSON?) {
        removeUserData()
        let user = UserE()
        user.setData(data)
        
        try! realm.write {
            realm.add(user)
        }
    }
    
    func userProfileInfo(_ callback: (UserE)->()) {
        let users = realm.objects(UserE)
        callback(users.first!)
    }
    
    func removeUserData() {
        print("Eliminando datos del usuario...")
        try! realm.write {
            realm.delete(realm.objects(UserE))
        }
    }
}
