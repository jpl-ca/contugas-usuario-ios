//
//  EventContuclubService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/27/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class EventContuclubService {
    let apiHelper = ApiHelper()
    var eventList:[EventE] = []
    
    func getEvents(_ callback: @escaping ([EventE])->()) {
        let service = "/api/user/auth/me/events?type=event"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(self.eventList)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.parseEvents(data!)
                print(self.eventList.count)
                callback(self.eventList)
            }
        }
    }
    
    func parseEvents(_ data:JSON){
        let eventArray = data["data"].arrayValue
        self.eventList = [EventE]()
        
        for val in eventArray {
            let event = EventE()
            event.event_id = String(val["id"].int!)
            if(val["constraints"].null == nil){
                event.constraints = val["constraints"].string!
            }
            if(val["content"].null == nil){
                event.content = val["content"].string!
            }
            event.image_url = val["image_url"].string!
            event.title = val["title"].string!
            event.date = val["date"].string!
            event.attending = val["attending"].bool!
            
            eventList.append(event)
        }
    }
    
    func imGoingToAttend(_ event_id:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events/\(event_id)/participations"
        let params = ["state":"1"]
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func imNotGoingToAttend(_ event_id:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events/\(event_id)/participations"
        apiHelper.makeDeleteRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
