//
//  JMStore.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class JMStore{
    
    let PREF_BANNER_CONTUGAS = "PREF_BANNER_CONTUGAS";
    let PREF_APP_WAS_INSTALLED = "PREF_APP_WAS_INSTALLED";
    let PREF_BANNER_WAS_DOWNLOADED = "PREF_BANNER_WAS_DOWNLOADED";
    let PREF_APP_HAD_SESSION = "PREF_APP_HAD_SESSION";
    let PREF_BANNER_MAIN_SEEN = "PREF_BANNER_MAIN_SEEN";
    
    
    
    func appWasInstalled() -> Bool{
        let installed:AnyObject? = getData(PREF_APP_WAS_INSTALLED)
        saveData(PREF_APP_WAS_INSTALLED, value: "1" as AnyObject)
        return installed == nil ? false : String(describing: installed!) == "1"
    }
    
    func bannerWasDownloaded() -> (Bool) {
        let downloaded:AnyObject? = getData(PREF_BANNER_WAS_DOWNLOADED)
        return downloaded == nil ? false : String(describing: downloaded!) == "1"
    }
    func bannerMainSeen() -> (Bool) {
        let seen:AnyObject? = getData(PREF_BANNER_MAIN_SEEN)
        return seen == nil ? false : String(describing: seen!) == "1"
    }
    func bannerMainSeen(_ seen:Bool) {
        saveData(PREF_BANNER_MAIN_SEEN,value: seen as AnyObject)
    }
    
    func bannerWasDownloaded(_ downloaded:Bool) {
        saveData(PREF_BANNER_WAS_DOWNLOADED, value: downloaded as AnyObject)
    }
    
    func appHadSession() -> (Bool){
        let hadSession:AnyObject? = getData(PREF_APP_HAD_SESSION)
        saveData(PREF_APP_HAD_SESSION, value: "1" as AnyObject)
        return hadSession == nil ? false : String(describing: hadSession!) == "1"
    }
    
    func appCloseSession(){
        removeData(PREF_APP_HAD_SESSION)
        removeData(PREF_BANNER_WAS_DOWNLOADED)
        removeData(PREF_BANNER_MAIN_SEEN)
    }
    
    
    func saveData(_ key:String,value:AnyObject){
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: key)
        userDefaults.synchronize()
    }
    
    func getData(_ key:String) -> AnyObject?{
        if let val: AnyObject = UserDefaults.standard.object(forKey: key) as AnyObject? {
            return val
        }
        return nil
    }
    
    func removeData(_ key:String){
        UserDefaults.standard.removeObject(forKey: key)
    }
}
