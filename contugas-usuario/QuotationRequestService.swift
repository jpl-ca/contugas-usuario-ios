//
//  QuotationRequestService.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/7/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class QuotationRequestService {
    let apiHelper = ApiHelper()
    var provinceList = [ProvinceE]()
    var provincesInfo: [String:ProvinceE] = [:]
    var districtsInfo: [String:DistrictE] = [:]
    
    let locationHelper = LocationHelper()
    
    init(){
        locationHelper.configLocationManager()
    }
    
    func sendRequest(_ nombres:String,apellidos:String,ubicacion:String,provincia:String,
         distrito:String, telefono:String, email:String,
         comentario:String, callback: @escaping (Bool)->()) {
        let service = "/api/quotation-requests"
        
        let currLocation = locationHelper.getLocation()
        let lat = currLocation.0
        let lng = currLocation.1
        
        let params = ["name": nombres, "last_name": apellidos, "address": ubicacion, "province_id":provincia, "district_id":distrito, "phone":telefono, "email":email, "comment":comentario,"lat":lat,"lng":lng]
        
        print("Enviando...")
        print(params)
        
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func getUbigeo(_ callback: @escaping (Bool,[String:ProvinceE],[String:DistrictE])->()) {
        let service = "/api/ubigeo/departaments/10?fields=districts"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false,self.provincesInfo,self.districtsInfo)
            }else{
//                print("JSONN: \(data)")
                print("D:****************************")
                self.parseProvinces(data)
                callback(true,self.provincesInfo,self.districtsInfo)
            }
        }
    }
    
    
    func parseProvinces(_ data:JSON?){
        let provinceArray = data!["data"]["provinces"].arrayValue
        
        for val in provinceArray {
            let province = ProvinceE()
            province.name = val["name"].string!
            province.code = val["code"].int!
            province.district = parseDistricts(val["districts"])
            provinceList.append(province)
            let key = String(province.code)
            provincesInfo[key] = province
        }
    }
    
    func parseDistricts(_ districtJson:JSON)->([DistrictE]){
        var districtList = [DistrictE]()
        let districtArray = districtJson.arrayValue
        for val in districtArray {
            let district = DistrictE()
            district.name = val["name"].string!
            district.code = val["code"].int!
            district.parent = val["parent"].string!
            districtList.append(district)
            let key = String(district.code)
            districtsInfo[key] = district
        }
        return districtList
    }
}
