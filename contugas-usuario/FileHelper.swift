//
//  FileHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/6/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import UIKit

class FileHelper{
    var imagePath = ""
    
    func saveImage (_ image: UIImage, nameImage: String ) -> Bool{
        imagePath = fileInDocumentsDirectory(nameImage)
        let pngImageData = UIImagePNGRepresentation(image)
        let result = (try? pngImageData!.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])) != nil
        return result
    }
    
    func loadImageFromPath(_ nameImage: String) -> UIImage? {
        imagePath = fileInDocumentsDirectory(nameImage)
        let image = UIImage(contentsOfFile: imagePath)
        if image == nil {
            print("missing image at: \(imagePath)")
        }
        print("Loading image from path: \(imagePath)")
        return image
    }
    
    
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> String {
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL.path
        
    }
}
