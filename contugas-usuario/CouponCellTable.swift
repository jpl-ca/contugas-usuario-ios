//
//  CouponCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class CouponCellTable: UITableViewCell {

    let downloadService = DownloadService()
    @IBOutlet weak var iv_event: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ image:String){
        downloadService.downloadImage(image){
            (image) -> () in
            self.iv_event.image = image
        }
    }
}
