//
//  ProvinceE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/28/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class ProvinceE{
    dynamic var code:Int = 0
    dynamic var name:String = ""
    var district = [DistrictE]()
}