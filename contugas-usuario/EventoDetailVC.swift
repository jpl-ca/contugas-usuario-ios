//
//  EventoDetailVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class EventoDetailVC: UIViewController {
    var evento : EventE = EventE()
    let downloadService = DownloadService()
    
    @IBOutlet weak var imgEvento: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var txtFecha: UILabel!
    @IBOutlet weak var txtAttend: UILabel!
    @IBOutlet weak var swAttendEvent: UISwitch!
    
    let Asistire = "Asistiré a este evento"
    let Asistir = "Asistir a este evento"
    
    weak var delegate: EventDetailDelegate? = nil
    
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        swAttendEvent.tintColor = colorHelper.colorPrimary()
        swAttendEvent.onTintColor = colorHelper.colorPrimary()
        
        downloadService.downloadImage((evento.image_url)){
            (image) -> () in
            self.imgEvento.image = image
        }
        
        txtTitle.text = evento.title
        txtFecha.text = evento.date
        txtDescription.text = "\(evento.content) \n \(evento.constraints)"
        swAttendEvent.setOn(evento.attending, animated: true)
        if(evento.attending){
            txtAttend.text = Asistire
        }else{
            txtAttend.text = Asistir
        }
    }
    
    @IBAction func asistirEvento(_ sender:UISwitch){
        if(sender.isOn){
            txtAttend.text = Asistire
        }else{
            txtAttend.text = Asistir
        }
    }
    
    func goBack(_ sender:UIButton){
        delegate?.attendToEvent(swAttendEvent.isOn)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupToolbar(){
        styleHelper.setNavigationBarStyle(self.navigationController!)
        
        navigationItem.title = "Evento"
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(EventoDetailVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }

}

protocol EventDetailDelegate: class {
    func attendToEvent(_ attend: Bool)
}
