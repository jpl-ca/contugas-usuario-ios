//
//  PaymentPlaceE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class PlaceE{
    dynamic var name:String = ""
    dynamic var address:String = ""
    
    dynamic var lat:Double = 0
    dynamic var lng:Double = 0
    
    dynamic var phone:String = ""
    dynamic var attention_hour:String = ""
    dynamic var city:String = ""
}