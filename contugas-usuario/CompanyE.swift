//
//  CompanyE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/27/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift

class CompanyE{
    dynamic var id:Int = 0
    dynamic var ruc:String = ""
    dynamic var address:String = ""
    dynamic var name:String = ""
    dynamic var description:String = ""
    dynamic var lat:Double = 0
    dynamic var lng:Double = 0
}