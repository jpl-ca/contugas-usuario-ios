//
//  UtilHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/22/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class UtilHelper{
    func formatToDecimal(_ value:Double,decimals:Int) -> (String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = decimals
        formatter.maximumFractionDigits = decimals
        return formatter.string(from: NSNumber(value))!
    }
    
    func formatToSoles(_ str:String) -> String{
        if str.range(of: "S") != nil{
            return str
        }
        return "S/.\(str)"
    }
    
    func parseToDouble(_ str:String) -> Double{
        if(str==""){
            return 0.0
        }
        return Double(str)!
    }
    
    func parseToInt(_ str:String) -> Int{
        if(str==""){
            return 0
        }
        return Int(str)!
    }
}
