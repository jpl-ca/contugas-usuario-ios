//
//  ProfileVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    let colorHelper = ColorHelper()
    let userProfile = UserProfileService()
    let logoutService = LogoutService()
    let styleHelper = StyleHelper()
    
    @IBOutlet weak var btnCerrarSesion:UIButton!
    @IBOutlet weak var btnCambiarPass:UIButton!
    @IBOutlet weak var txt_numero_cliente:UILabel!
    @IBOutlet weak var txt_titular_servicio:UILabel!
    @IBOutlet weak var txt_direccion:UILabel!
    @IBOutlet weak var txt_distrito:UILabel!
    @IBOutlet weak var txt_provincia:UILabel!
    @IBOutlet weak var txt_estado_suministro:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCerrarSesion.backgroundColor = colorHelper.colorGreenButton()
        btnCambiarPass.tintColor = colorHelper.colorGreenButton()
        setupToolbar()
        showUserInfo()
    }
    
    func showUserInfo(){
        userProfile.userProfileInfo(){ (user) -> () in
            self.txt_numero_cliente.text = user.client_number
            self.txt_titular_servicio.text = user.name
            self.txt_direccion.text = user.address
            self.txt_estado_suministro.text = user.supply_state
            self.txt_distrito.text = user.district_text
            self.txt_provincia.text = user.province_text
        }
    }
    
    @IBAction func logoutTapped (_ sender:UIButton) {
        logoutService.logout(){(success_logout) -> () in
            self.dismiss(animated: true, completion: nil)
        }
    }

    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = VAR.TitleVC.mi_cuenta
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(ProfileVC.goBack(_:)), for: .touchUpInside)
        
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
