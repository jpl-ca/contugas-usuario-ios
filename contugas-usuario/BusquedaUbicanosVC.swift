//
//  BusquedaUbicanosVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class BusquedaUbicanosVC: UIViewController, UITableViewDataSource, UITableViewDelegate,SelectPlaceDelegate{
    @IBOutlet weak var tb_list_place: UITableView!
    weak var delegate: SearchPlaceDelegate? = nil
    var placeList: [PaymentAttentionPlaceE] = [PaymentAttentionPlaceE]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tb_list_place.delegate = self
        tb_list_place.dataSource = self
        tb_list_place.reloadData()
        tb_list_place.allowsSelection = false
        tb_list_place.alwaysBounceVertical = false
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let pl = placeList[indexPath.row].places
        return 45.0 + 55.0 * CGFloat(pl.count)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ListPlaceCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ListPlaceCellTable
        
        cell.setCell(placeList[indexPath.row])
        cell.setIndex(indexPath.row)
        cell.setDelegate(self)
        
        return cell
    }
    
    func placeSelected(_ place : PlaceE, placeType: PLACE_TYPE,idxArea:Int,idxPlaceInArea:Int){
        delegate?.placeSelected(place,placeType: placeType,idxArea:idxArea,idxPlaceInArea:idxPlaceInArea)
        self.navigationController?.popViewController(animated: true)
    }
}

protocol SearchPlaceDelegate: class {
    func placeSelected(_ place: PlaceE, placeType: PLACE_TYPE, idxArea:Int, idxPlaceInArea:Int)
}
