//
//  CustomInfoWindowUbicanos.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/2/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class CustomInfoWindowUbicanos: UIView {
    let margin_horizontal = CGFloat(8)
    var pos = 4
    
    func addLabel(_ str:String,align:NSTextAlignment,title:Bool){
        var fontSize = 12
        if(title){
            fontSize = 15
            pos = pos + 4
        }
        
        let width = self.frame.width - 2 * margin_horizontal
        let width_text = Int(width/7)
        let numberOfLines = (str.characters.count / width_text)+1
        let height = CGFloat(numberOfLines * 21)
        
        let label = UILabel(frame: CGRect(x: margin_horizontal, y: CGFloat(pos), width: width, height: height))
        label.textAlignment = align
        label.text = str
        
        label.font = UIFont(name: label.font.fontName, size: CGFloat(fontSize))
        
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = numberOfLines
        
        self.addSubview(label)
        pos = pos + numberOfLines * 18
    }
}
