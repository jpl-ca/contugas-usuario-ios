//
//  ZoneE.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class ZoneE{
    dynamic var lat:Double = 0
    dynamic var lng:Double = 0
    var zones:[[ZonePointE]] = [[ZonePointE]]()
}