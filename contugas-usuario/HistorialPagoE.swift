//
//  HistorialPagoE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class HistorialPagoE{
    dynamic var fecha:String = ""
    dynamic var lugar:String = ""
    dynamic var importe:String = ""
    
    init(fecha:String,lugar:String,importe:String){
        self.fecha = fecha
        self.lugar = lugar
        self.importe = importe
    }
    
    init(data:JSON?){
        self.fecha = data!["fecha"].string!
        self.lugar = data!["lugar"].string!
        self.importe = data!["importe"].string!
    }
}