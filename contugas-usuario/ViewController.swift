//
//  ViewController.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate {
    let splashService = SplashService()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        checkSessionState()
    }
    
    func checkSessionState(){
        splashService.checkSessionStatus(){(isLoggedIn) -> () in
            if isLoggedIn {
                self.goToUserPanel()
            } else {
                self.performSegue(withIdentifier: "goto_panel", sender: self)
            }
        }
    }
    
    func goToUserPanel(){
        let storyboard = UIStoryboard(name: "UserPanel", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserPanelSB") as UIViewController
        present(controller, animated: true, completion: nil)
    }
}
