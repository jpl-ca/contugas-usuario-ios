//
//  ConsejoCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ConsejoCellTable: UITableViewCell {

    let downloadService = DownloadService()
    @IBOutlet weak var iv_consejo: UIImageView!
    @IBOutlet weak var txt_title: UILabel!
    
    var delegate:ConsejoCellDelegate? = nil
    var advice = AdviceE()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ _advice:AdviceE,_delegate:ConsejoCellDelegate){
        self.advice = _advice
        txt_title.text = self.advice.title
        downloadService.downloadImage(advice.image_url){
            (image) -> () in
            self.iv_consejo.image = image
        }
        self.delegate = _delegate
    }
    
    @IBAction func shareImage(_ sender:UIButton){
        delegate?.onShareImage(advice.image_url,title: advice.title)
    }
    
}

protocol ConsejoCellDelegate: class {
    func onShareImage(_ image: String,title:String)
}
