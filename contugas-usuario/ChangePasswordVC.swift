//
//  ChangePasswordVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/18/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    let changePasswordService = ChangePasswordService()
    let alertDialog = AlertDialogHelper()
    @IBOutlet weak var btn_cambiar_pass: UIButton!
    @IBOutlet weak var txtPasswordActual:UITextField!
    @IBOutlet weak var txtPasswordNueva:UITextField!
    @IBOutlet weak var txtRepetirPasswordNueva:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        btn_cambiar_pass.backgroundColor = colorHelper.colorGreenButton()
        styleHelper.setBorder(txtPasswordActual)
        styleHelper.setBorder(txtPasswordNueva)
        styleHelper.setBorder(txtRepetirPasswordNueva)
    }
    
    @IBAction func changePassword(_ sender:UIButton){
        let passActual = txtPasswordActual.text!
        let passNueva = txtPasswordNueva.text!
        let passRepetirNuevo = txtRepetirPasswordNueva.text!
        if(passActual == "" || passNueva == "" || passRepetirNuevo == ""){
            self.alertDialog.showAlert(self,title: "Contugas",message: "Todos los datos son necesarios!")
        }else if(passNueva != passRepetirNuevo){
            self.alertDialog.showAlert(self,title: "Contugas",message: "Contraseñas ingresadas nueva y repetir contraseña no coinciden")
        }else{
            changePasswordService.changePassword(passActual, newPass: passNueva){
                (res) -> () in
                if(res){
                    self.alertDialog.showAlert(self,title: "Contugas",message: "Su información fue actualizada!"){
                        (b) -> () in
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    self.alertDialog.showAlert(self,title: "Contugas",message: "Su clave actual ingresada no es correcta")
                }
            }
        }
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        navigationItem.title = "Actualizar Contraseña"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(ChangePasswordVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    @IBAction func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
