//
//  ConsumptionStore.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/4/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift

class ConsumptionStore {
    let realm = try! Realm()
    
    func storeConsumption(_ data:ConsumptionInvoiceE) {
        try! realm.write {
            realm.delete(realm.objects(ConsumptionInvoiceE))
        }
        try! realm.write {
            realm.add(data)
        }
    }
    
    func storeConsumptions(_ data:[ConsumptionInvoiceE]) {
        try! realm.write {
            realm.delete(realm.objects(ConsumptionInvoiceE))
        }
        try! realm.write {
            realm.add(data)
        }
    }
    
    func getConsumption(_ callback: (Array<ConsumptionInvoiceE>)->()) {
        var consumptionList = Array<ConsumptionInvoiceE>()
        let consumptions = realm.objects(ConsumptionInvoiceE)
        for consumption in consumptions {
            consumptionList.append(consumption)
        }
        callback(consumptionList)
    }
}
