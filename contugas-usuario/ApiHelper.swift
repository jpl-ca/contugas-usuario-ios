//
//  ApiHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/26/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiHelper {
    
//    let base_url = "http://45.55.200.155:9002"
    let base_url = "http://181.224.251.179"
    
    func makeGetRequest(_ service: String, callback: (JSON?, NSError?)->()){
        Alamofire.request(.GET, base_url+service, parameters: nil, encoding: ParameterEncoding.url).responseJSON { (request, response, result) in
            var _json:JSON = nil
            var _error:NSError = NSError(domain: self.base_url, code: 0, userInfo: ["msg":"No se puede conectar al servidor"])
            if(response != nil){
                let status_code:Int = response!.statusCode
                print("--->>\(status_code)")
                switch result {
                case .success(let data):
                    if(status_code>=200 && status_code<300){
                        _json = JSON(data)
                    }else{
                        let msg = data
                        _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                    }
                case .failure(_, let error):
                    let msg = "Request failed with error: \(error)"
                    _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                }
            }
            callback(_json,_error)
        }
    }
    
    func makeDeleteRequest(_ service: String, callback: (JSON?, NSError?)->()){
        Alamofire.request(.DELETE, base_url+service, parameters: nil, encoding: ParameterEncoding.url).responseJSON { (request, response, result) in
            var _json:JSON = nil
            var _error:NSError = NSError(domain: self.base_url, code: 0, userInfo: ["msg":"No se puede conectar al servidor"])
            if(response != nil){
                let status_code:Int = response!.statusCode
                print("--->>\(status_code)")
                switch result {
                case .success(let data):
                    if(status_code>=200 && status_code<300){
                        _json = JSON(data)
                    }else{
                        let msg = data
                        _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                    }
                case .failure(_, let error):
                    let msg = "Request failed with error: \(error)"
                    _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                }
            }
            callback(_json,_error)
        }
    }
    
    
    
    func makePutRequest(_ service: String, params:[String:AnyObject], callback: (JSON?, NSError?)->()){
        Alamofire.request(.PUT, base_url+service, parameters: params, encoding: ParameterEncoding.url).responseJSON { (request, response, result) in
            var _json:JSON = nil
            var _error:NSError = NSError(domain: self.base_url, code: 0, userInfo: ["msg":"No se puede conectar al servidor"])
            if(response != nil){
                let status_code:Int = response!.statusCode
                print("--->>\(status_code)")
                switch result {
                case .success(let data):
                    if(status_code>=200 && status_code<300){
                        _json = JSON(data)
                    }else{
                        let msg = data
                        _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                    }
                case .failure(_, let error):
                    let msg = "Request failed with error: \(error)"
                    _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                }
            }
            callback(_json,_error)
        }
    }
    
    func makePostRequest(_ service: String, params:[String:AnyObject], callback: (JSON?, NSError?)->()){
        Alamofire.request(.POST, base_url+service, parameters: params, encoding: ParameterEncoding.url).responseJSON { (request, response, result) in
            var _json:JSON = nil
            var _error:NSError = NSError(domain: self.base_url, code: 0, userInfo: ["msg":"No se puede conectar al servidor"])
            if(response != nil){
                let status_code:Int = response!.statusCode
                print("--->>\(status_code)")
                switch result {
                case .success(let data):
                    if(status_code>=200 && status_code<300){
                        _json = JSON(data)
                    }else{
                        let msg = data
                        _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                    }
                case .failure(_, let error):
                    let msg = "Request failed with error: \(error)"
                    _error = NSError(domain: self.base_url, code: status_code, userInfo: ["msg":msg])
                }
            }
            callback(_json,_error)
        }
    }
    
    func downloadImage(_ url_img: String, callback: @escaping (UIImage)->()){
        Alamofire.request(.GET, url_img).response() {
            (_, _, data, _) in
            let image = UIImage(data: data!)
            callback(image!)
        }
    }
}
