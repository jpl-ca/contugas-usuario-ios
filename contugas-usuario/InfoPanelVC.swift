//
//  InfoPanelVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/7/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class InfoPanelVC: UIViewController {
    let colorHelper = ColorHelper()
    
    @IBOutlet weak var v_linea_emergencia:UIView!
    @IBOutlet weak var v_alo_contugas:UIView!
    @IBOutlet weak var v_enviar_correo:UIView!
    @IBOutlet weak var v_ubicanos:UIView!
    @IBOutlet weak var v_zonagas:UIView!
    @IBOutlet weak var v_sugerencia:UIView!

    let styleHelper = StyleHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        setupView()
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView(){
        let tapEmergencia = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleTapEmergencia(_:)))
        v_linea_emergencia.addGestureRecognizer(tapEmergencia)
        
        let tapAloContugas = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleTapAloContugas(_:)))
        v_alo_contugas.addGestureRecognizer(tapAloContugas)
        
        let tapEnviarCorreo = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleTapEnviarCorreo(_:)))
        v_enviar_correo.addGestureRecognizer(tapEnviarCorreo)
        
        let tapUbicanos = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleTapUbicanos(_:)))
        v_ubicanos.addGestureRecognizer(tapUbicanos)
        
        let tapZonaGas = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleTapZonaGas(_:)))
        v_zonagas.addGestureRecognizer(tapZonaGas)
        
        let tapSugerencia = UITapGestureRecognizer(target: self, action: #selector(InfoPanelVC.handleSugerencia(_:)))
        v_sugerencia.addGestureRecognizer(tapSugerencia)
    }
    
    func handleTapEmergencia(_ sender: UITapGestureRecognizer){
        print("Calling...")
        UIApplication.shared.openURL(URL(string: "tel://056600600")!)
    }
    func handleTapAloContugas(_ sender: UITapGestureRecognizer){
        print("Call...")
        UIApplication.shared.openURL(URL(string: "tel://056531919")!)
    }
    func handleTapEnviarCorreo(_ sender: UITapGestureRecognizer){
        let email = "alocontugas@contugas.com.pe"
        let url = URL(string: "mailto:\(email)")
        UIApplication.shared.openURL(url!)
    }
    func handleTapUbicanos(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_ubicanos", sender: self)
    }
    func handleTapZonaGas(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_zona_gas", sender: self)
    }
    func handleSugerencia(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_suggestion", sender: self)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Mas Información"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(InfoPanelVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
