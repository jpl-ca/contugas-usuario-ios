//
//  RegisterIncidentVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit
import CoreLocation

class RegisterIncidentVC: UIViewController,UITextViewDelegate{
    let registerIncidentService = RegisterIncidentService()
    @IBOutlet weak var txtType:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtNumAddress:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    
    var type_selected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtDescription.delegate = self
        registerIncidentService.initialConfig()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func showAlertIncidentTypes(){
        let alert = UIAlertController(title: "Tipo de Incidente", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let iTypes = registerIncidentService.getIncidentTypes()
        
        for (key,value) in iTypes {
            alert.addAction(UIAlertAction(title: "\(value)", style: UIAlertActionStyle.default, handler: {
                action in
                self.type_selected = key
                self.txtType.text = value
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func selectTypeTapped(_ sender:AnyObject){
        self.showAlertIncidentTypes()
    }
    
    @IBAction func sendTapped(_ sender:UIButton){
        let type:NSString = type_selected as NSString
        let address:NSString = txtAddress.text! as NSString
        let number:NSString = txtNumAddress.text! as NSString
        let description:NSString = txtDescription.text! as NSString
        
        if type_selected == "" {
            self.showAlert("Debe seleccionar un tipo de incidente.")
            return
        }
        
        if address == "" {
            self.showAlert("Debe ingresar una dirección válida.")
            return
        }
        
        if number == "" {
            self.showAlert("Debe ingresar el numero de la dirección.")
            return
        }
        
        if description == "" {
            self.showAlert("Debe ingresar una descripción.")
            return
        }
        
        registerIncidentService.sendIncident(type as String, address: address as String, number: number as String, desc: description as String){
            (res)->() in
            if res {
                self.showAlertRequest("El incidente fue registrado, Gracias.")
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlertRequest(_ message:String){
        let alert = UIAlertController(title: "Contugas", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{action in self.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(_ message:String){
        let alert = UIAlertController(title: "Contugas", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
