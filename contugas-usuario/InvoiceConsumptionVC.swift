//
//  InvoiceConsumptionVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class InvoiceConsumptionVC: UIViewController {
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let utilHelper = UtilHelper()
    let consumptionInvoiceService = ConsumptionInvoiceService()
    let user = UserProfileService()
    let bannerContugasService = BannerContugasService()
    let alertDialogHelper = AlertDialogHelper()
    
    @IBOutlet weak var v_historico:UIView!
    @IBOutlet weak var v_pagos:UIView!
    @IBOutlet weak var v_financiamiento:UIView!
    
    @IBOutlet weak var txt_mes: UILabel!
    @IBOutlet weak var txtPeriod:UILabel!
    @IBOutlet weak var txtExpireDate:UILabel!
    @IBOutlet weak var txtNumerAccount:UILabel!
    @IBOutlet weak var txtCategoryRate:UILabel!
    @IBOutlet weak var txtVolumenInvoice:UILabel!
    
    @IBOutlet weak var txCurrentDebt:UILabel!
    @IBOutlet weak var txLastDebt:UILabel!
    @IBOutlet weak var txTotalInvoice:UILabel!
    
    @IBOutlet weak var btn_download_pdf: UIButton!
    @IBOutlet weak var btnPrevious:UIButton!
    @IBOutlet weak var btnNext:UIButton!
    
    @IBOutlet weak var iv_banner_contugas: UIImageView!
    
    var idx = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCurrentDate()
        setupToolbar()
        setupView()
        loadData()
        loadBanner()
    }
    
    func setupCurrentDate(){
        var months:[String] = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
        
        let date = Date()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day , .month , .year], from: date)
        let month = components.month
        
        txt_mes.text = months[month! - 1].uppercased() // Mes Actual
    }
    
    @IBAction func nextMonth(_ sender:UIButton){
        consumptionInvoiceService.nextConsumption(){ (consumo,has_prev,has_next) -> () in
            self.btnPrevious.isEnabled = has_prev
            self.btnNext.isEnabled = has_next
            self.showConsumption(consumo)
        }
    }
    @IBAction func previousMonth(_ sender:UIButton){
        consumptionInvoiceService.prevConsumption(){ (consumo,has_prev,has_next) -> () in
            self.btnPrevious.isEnabled = has_prev
            self.btnNext.isEnabled = has_next
            self.showConsumption(consumo)
        }
    }
    
    func loadData(){
        user.userProfileInfo(){
            (user) -> () in
            self.txtNumerAccount.text = user.client_number
        }
        
        consumptionInvoiceService.getConsumptionInvoice(){ (success) -> () in
            if(success){
                self.consumptionInvoiceService.lastConsumption(){ (consumo,has_prev,has_next) -> () in
                    self.btnPrevious.isEnabled = has_prev
                    self.btnNext.isEnabled = has_next
                    self.showConsumption(consumo)
                }
            }else{
                
            }
        }
    }
    
    func showConsumption(_ consumo:ConsumptionInvoiceE) {
        txt_mes.text = consumo.month_name
        txt_mes.text = consumo.month_name
        txtPeriod.text = consumo.period
        txtExpireDate.text = consumo.expiration_date
        
        txtCategoryRate.text = consumo.rate_category
        
        txtVolumenInvoice.text = consumo.volume_invoice
        
        let lastDebt = utilHelper.formatToDecimal(utilHelper.parseToDouble(consumo.last_debt), decimals: 2)
        txLastDebt.text = utilHelper.formatToSoles(lastDebt)
        
        let currentDebt = utilHelper.formatToDecimal(utilHelper.parseToDouble(consumo.current_debt), decimals: 2)
        txCurrentDebt.text = utilHelper.formatToSoles(currentDebt)
        
        let total_str = utilHelper.formatToDecimal(utilHelper.parseToDouble(consumo.total_invoice), decimals: 2)
        txTotalInvoice.text = utilHelper.formatToSoles(total_str)
    }
    
    @IBAction func downloadPDF(){
        let msg = VAR.Message.downloading_invoice
        let alertController = alertDialogHelper.showLoading(msg)
        self.present(alertController, animated: false, completion: nil)
        consumptionInvoiceService.downloadInvoicePdf(){
            (b) -> () in
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    func loadBanner(){
        bannerContugasService.loadBannerInvoice(iv_banner_contugas)
    }
    
    func setupView(){
        let tapHistorico = UITapGestureRecognizer(target: self, action: #selector(InvoiceConsumptionVC.handleTapHistorico(_:)))
        v_historico.addGestureRecognizer(tapHistorico)
        
        let tapPagos = UITapGestureRecognizer(target: self, action: #selector(InvoiceConsumptionVC.handleTapPagos(_:)))
        v_pagos.addGestureRecognizer(tapPagos)
        
        let tapFinanciamiento = UITapGestureRecognizer(target: self, action: #selector(InvoiceConsumptionVC.handleTapFinanciamiento(_:)))
        v_financiamiento.addGestureRecognizer(tapFinanciamiento)
        
        btn_download_pdf.backgroundColor = colorHelper.colorGreenButton()
    }
    
    
    func handleTapHistorico(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_invoice_historico", sender: self)
        
    }
    func handleTapPagos(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_invoice_pagos", sender: self)
    }
    func handleTapFinanciamiento(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "goto_invoice_financiamiento", sender: self)
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        navigationItem.title = "Recibo"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(InvoiceConsumptionVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }

}
