//
//  ContuclubVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class CouponContuclubVC: UIViewController , UITableViewDelegate, UITableViewDataSource{
    let couponsService = CouponsContuclubService()
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    
    @IBOutlet var tableViewCupones: UITableView!
    var itemsC: [CouponE] = []
    var currentCoupon: CouponE?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewCupones.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        couponsService.loadCoupons(){
            (_items) -> () in
            self.setupButtonPlaces()
            self.itemsC = _items
            self.tableViewCupones.reloadData()
        }
        setupToolbar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController2 = segue.destination as? CouponDetailVC {
            viewController2.coupon = currentCoupon
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsC.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CouponCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CouponCellTable
        
        cell.setCell(self.itemsC[indexPath.row].img_url)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentCoupon = itemsC[indexPath.row]
        self.performSegue(withIdentifier: "goto_coupon_detail", sender: self)
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Cupones"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(CouponContuclubVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    
    
    
    
    
    
    func setupButtonPlaces() {
        addMenuOptionZone(1,name:"Nasca",image:"nasca",color:colorHelper.colorNasca(),buttonAction:#selector(CouponContuclubVC.BuscarNasca(_:)))
        addMenuOptionZone(2,name:"Ica",image:"ica",color:colorHelper.colorIca(),buttonAction:#selector(CouponContuclubVC.BuscarIca(_:)))
        addMenuOptionZone(3,name:"Pisco",image:"pisco",color:colorHelper.colorPisco(),buttonAction:#selector(CouponContuclubVC.BuscarPisco(_:)))
        addMenuOptionZone(4,name:"Chincha",image:"chincha",color:colorHelper.colorChincha(),buttonAction:#selector(CouponContuclubVC.BuscarChincha(_:)))
    }
    
    func BuscarIca(_ sender:UIButton) {
        couponsService.loadCouponsByProvince(VAR.ProvincesCode.code_ica){
            (_items) -> () in
            self.itemsC = _items
            self.tableViewCupones.reloadData()
            self.navigationItem.title = "\(VAR.Cupones) - \(VAR.ProvincesName.ica)"
        }
    }
    func BuscarPisco(_ sender:UIButton) {
        couponsService.loadCouponsByProvince(VAR.ProvincesCode.code_pisco){
            (_items) -> () in
            self.itemsC = _items
            self.tableViewCupones.reloadData()
            self.navigationItem.title = "\(VAR.Cupones) - \(VAR.ProvincesName.pisco)"
        }
    }
    func BuscarNasca(_ sender:UIButton) {
        couponsService.loadCouponsByProvince(VAR.ProvincesCode.code_nasca){
            (_items) -> () in
            self.itemsC = _items
            self.tableViewCupones.reloadData()
            self.navigationItem.title = "\(VAR.Cupones) - \(VAR.ProvincesName.nasca)"
        }
    }
    func BuscarChincha(_ sender:UIButton) {
        couponsService.loadCouponsByProvince(VAR.ProvincesCode.code_chincha){
            (_items) -> () in
            self.itemsC = _items
            self.tableViewCupones.reloadData()
            self.navigationItem.title = "\(VAR.Cupones) - \(VAR.ProvincesName.chincha)"
        }
    }
    
    func addMenuOptionZone(_ pos:Int, name:String, image:String, color:UIColor, buttonAction:Selector){
        let h = 45
        let w = 45
        let space_button_map = 75
        let paddingRight = CGFloat(55)
        let paddingBottom = CGFloat(space_button_map + ( ( h + 10 ) * pos ) )
        
        let X_Pos = self.view.frame.size.width - paddingRight
        let Y_Pos = self.view.frame.size.height - paddingBottom
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: X_Pos, y: Y_Pos, width: CGFloat(w), height: CGFloat(h))
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.backgroundColor = color
        button.setImage(UIImage(named:image), for: UIControlState())
        button.imageEdgeInsets = UIEdgeInsetsMake(35,35,35,35)
        button.addTarget(self, action: buttonAction, for: .touchUpInside)
        view.addSubview(button)
        addLabel(name, x: X_Pos - CGFloat( w + 27), y: Y_Pos + 12.5)
    }
    
    func addLabel(_ name:String,x:CGFloat,y:CGFloat){
        let label = UILabel(frame: CGRect(x: x, y: y, width: 70, height: 21))
        label.textAlignment = NSTextAlignment.center
        label.text = name
        
        label.backgroundColor = colorHelper.colorGrayDark()
        label.textColor = colorHelper.colorWhite()
        
        label.font = UIFont(name: label.font.fontName, size: 13)
        
        view.addSubview(label)
    }
}
