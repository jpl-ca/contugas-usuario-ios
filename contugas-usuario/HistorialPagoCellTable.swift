//
//  HistorialPagoCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class HistorialPagoCellTable: UITableViewCell {

    @IBOutlet weak var txt_fecha: UILabel!
    @IBOutlet weak var txt_lugar: UILabel!
    @IBOutlet weak var txt_pago: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txt_fecha.numberOfLines = 0;
    }
    
    func setCell(_ historialPago:HistorialPagoE){
        txt_fecha.text = historialPago.fecha
        txt_lugar.text = historialPago.lugar
        txt_pago.text = historialPago.importe
    }
}
