//
//  BannerContugasVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/6/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class BannerContugasVC: UIViewController {
    @IBOutlet weak var iv_banner: UIImageView!
    let styleHelper = StyleHelper()
    
    let userProfile = UserProfileService()
    var title_bar:String = ""
    let bannerContugasService = BannerContugasService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleHelper.setNavigationBarStyle(self.navigationController!)
        bannerContugasService.loadBannerMain(iv_banner)
        showUserInfo()
    }
    
    func showUserInfo(){
        userProfile.userFirstName(){ (name) -> () in
            self.title_bar = "\(name.capitalized), Bienvenido"
            self.navigationItem.title = self.title_bar
        }
    }
    
    @IBAction func close(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
}
