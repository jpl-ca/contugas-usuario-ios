//
//  StatisticVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/2/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit
import Charts
import CoreMotion

class StatisticVC: UIViewController,ChartViewDelegate {
    let consumptionInvoiceService = ConsumptionInvoiceService()
    @IBOutlet weak var chartView: BarChartView!
    var days:[String] = []
    var stepsTaken:[Int] = []
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    @IBOutlet weak var iv_banner: UIImageView!
    @IBOutlet weak var txt_mensaje: UILabel!
    @IBOutlet weak var txt_nota: UILabel!
    
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    let bannerContugasService = BannerContugasService()
    var cnt = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        chartView.delegate = self
        
        chartView.descriptionText = ""
        chartView.noDataTextDescription = "No hay información para mostrar"
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = true
        
        chartView.maxVisibleValueCount = 60
        chartView.pinchZoomEnabled = false
        chartView.drawGridBackgroundEnabled = false
        
        chartView.borderLineWidth = 0
        chartView.drawBordersEnabled = false
        
        self.navigationItem.title = VAR.TitleVC.consumo_historico
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        consumptionInvoiceService.getConsumptionList(){
            (consumptionList) -> () in
            self.setChart(consumptionList)
        }
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(StatisticVC.goBack(_:)), for: .touchUpInside)
        
        bannerContugasService.loadBannerHistory(iv_banner)
        txt_mensaje.text = consumptionInvoiceService.getMessageInvoiceSavedMoney()
        
        txt_nota.text = "* Se considera un volumen de consumo de 14m³ de gas natural. \n * No incluye el costo de las instalaciones internas, acometida y derecho de conexión. \n * El precio de S/. 38.00 soles del balon GLP de 10kg es referencial."
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
        
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func dismiss(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setChart(_ consumptionList:[ConsumptionInvoiceE]) {
        var months = Array<String>()
        var chartDataSets = Array<BarChartDataSet>()
        var dataEntriesM: [BarChartDataEntry] = []
        
        for i in 0..<consumptionList.count {
            let consumption:ConsumptionInvoiceE = consumptionList[i]
            months.append(consumption.month_name)
            let dataEntryM = BarChartDataEntry(value: Double(consumption.volume_invoice)!, xIndex: i)
            dataEntriesM.append(dataEntryM)
        }
        
        let chartDataSetM = BarChartDataSet(yVals: dataEntriesM, label: "Consumo m³")
        
        chartDataSets.append(chartDataSetM)
        
        chartDataSetM.colors = [colorHelper.colorSecondary()]
        
        let chartData = BarChartData(xVals: months, dataSets: chartDataSets)
        chartView.data = chartData
        chartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        self.view.reloadInputViews()
    }
}
