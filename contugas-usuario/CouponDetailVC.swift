//
//  CouponDetailVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/9/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit
import GoogleMaps

class CouponDetailVC: UIViewController {
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var txtCode: UILabel!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var imgCoupon: UIImageView!
    @IBOutlet weak var viewMap: GMSMapView!
    var qrcodeImage: CIImage!
    let downloadService = DownloadService()
    var coupon: CouponE?
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCode.text = coupon?.coupon_code
        txtTitle.text = coupon?.title
        txtDescription.text = coupon?.description
        downloadService.downloadImage((coupon?.img_url)!){
            (image) -> () in
            self.imgCoupon.image = image
        }
        genQRCode()
        setupToolbar()
        setupMap()
    }
    
    func genQRCode(){
        if qrcodeImage == nil {
            if txtCode.text == "" {
                return
            }
        }
        
        let data = txtCode.text!.data(using: String.Encoding.isoLatin1)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter!.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        
        qrcodeImage = filter!.outputImage!.applying(transform)
        
        imgQRCode.image = UIImage(ciImage: qrcodeImage)
        txtCode.resignFirstResponder()
    }
    
    func setupMap(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: (coupon?.company.lat)!, longitude: (coupon?.company.lng)!, zoom: 15.0)
        viewMap.camera = camera
        viewMap.settings.zoomGestures = true
        let  position = camera.target
        let marker = GMSMarker(position: position)
        marker.title = (coupon?.company.name)!
        marker.snippet = (coupon?.company.address)!
        marker.map = viewMap
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupToolbar(){
        styleHelper.setNavigationBarStyle(self.navigationController!)
        
        navigationItem.title = "Cupones"
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(CouponDetailVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
