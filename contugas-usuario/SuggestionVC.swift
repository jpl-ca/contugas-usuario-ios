//
//  SuggestionVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/11/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class SuggestionVC: UIViewController {
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let sugerenciaService = SugerenciaService()
    let alertDialogHelper = AlertDialogHelper()
    
    @IBOutlet weak var v_sugerencia:UITextView!
    @IBOutlet weak var btn_enviar:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        btn_enviar.backgroundColor = colorHelper.colorGreenButton()
        styleHelper.setBorder(v_sugerencia)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = VAR.TitleVC.sugerencias
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(SuggestionVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendSuggestion(_ sender:UIButton){
        let sugerencia = v_sugerencia.text
        let msg = VAR.Message.sending_suggestion
        let alertController = alertDialogHelper.showLoading(msg)
        self.present(alertController, animated: false, completion: nil)
        sugerenciaService.enviarSugerencia(sugerencia){
            (res) -> () in
            alertController.dismiss(animated: true, completion: nil)
            if(res){
                self.navigationController?.popViewController(animated: true)
            }else{
            }
        }
    }
}
