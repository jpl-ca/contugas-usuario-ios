//
//  BannerE.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class BannerE : Object{
    dynamic var id:Int = 0
    
    dynamic var MAIN:String = ""
    dynamic var INVOICE:String = ""
    dynamic var PAYMENT:String = ""
    dynamic var HISTORY:String = ""
    dynamic var FINANCING:String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}