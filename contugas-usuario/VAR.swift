//
//  VAR.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/5/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class VAR{
    
    static let Cupones = "Cupones"
    
    class ProvincesCode{
        static let code_ica = "1001"
        static let code_chincha = "1002"
        static let code_pisco = "1004"
        static let code_nasca = "1003"
    }
    
    class ProvincesName{
        static let ica = "Ica"
        static let chincha = "Chincha"
        static let pisco = "Pisco"
        static let nasca = "Nasca"
    }
    
    class TitleVC {
        static let zonas_gas_natural = "Zonas con Gas Natural"
        static let mi_cuenta = "Mi Cuenta"
        static let sugerencias = "Sugerencias"
        static let consumo_historico = "Consumo Histórico"
    }
    
    class Banner{
        static let MAIN = "MAIN"
    }
    
    class Button{
        static let Registrar = "Registrar"
    }
    
    class Message{
        static let downloading_invoice = "Descargando Recibo"
        static let invoice_saved_money = "Invoice_Saved_Money"
        static let sending_suggestion = "Enviando Sugerencia"
    }
}