//
//  ChangePasswordService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/28/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class ChangePasswordService {
    let apiHelper = ApiHelper()
    let userStore = UserStore()
    
    func changePassword(_ pass:String,newPass:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me"
        let params = ["last_password": pass,"new_password": newPass]
        apiHelper.makePutRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
