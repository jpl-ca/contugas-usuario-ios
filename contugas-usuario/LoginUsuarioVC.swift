//
//  LoginUsuarioVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/25/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class LoginUsuarioVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var v_quotation_request:UIView!
    @IBOutlet weak var v_information:UIView!
    let loginService = LoginService()
    let styleHelper = StyleHelper()
    let colorHelper = ColorHelper()
    let alertDialog = AlertDialogHelper()
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_register: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtPassword.delegate = self
        styleHelper.setBorderPrimary(txtEmail)
        styleHelper.setBorderPrimary(txtPassword)
        
        let tapQR = UITapGestureRecognizer(target: self, action: #selector(LoginUsuarioVC.handleTapQR(_:)))
        v_quotation_request.addGestureRecognizer(tapQR)
        
        let tapINF = UITapGestureRecognizer(target: self, action: #selector(LoginUsuarioVC.handleTapINF(_:)))
        v_information.addGestureRecognizer(tapINF)
        
        btn_login.backgroundColor = colorHelper.colorGreenButton()
        btn_register.tintColor = colorHelper.colorGreenButton()
    }
    func handleTapQR(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "QuotationRequest", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "QuotationRequestSB") as UIViewController
        present(controller, animated: true, completion: nil)
    }
    func handleTapINF(_ sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "InfoPanel", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InfoPanelSB") as UIViewController
        present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signinTapped(_ sender:UIButton){
        let email:NSString = txtEmail.text! as NSString
        let password:NSString = txtPassword.text! as NSString
        
        if (email.isEqual(to: "") || password.isEqual(to: "") ) {
            self.alertDialog.showAlert(self,title: "Contugas",message: "Email y Password, son necesarios para identificarlos")
        }else{
            loginService.LoginUser(email as String,password: password as String){(userSuccess) -> () in
                if userSuccess {
                    self.txtEmail.text = ""
                    self.txtPassword.text = ""
                    self.navigationController?.popToRootViewController(animated: true)
                } else {
                    self.alertDialog.showAlert(self,title: "Contugas",message: "Email o Password son incorrectos")
                }
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
