//
//  ConsejoVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ConsejoVC: UIViewController, UITableViewDataSource, UITableViewDelegate, ConsejoCellDelegate{
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let consejoService = ConsejoService()
    @IBOutlet var tb_consejo: UITableView!
    var consejos : [AdviceE] = [AdviceE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        tb_consejo.delegate = self
        tb_consejo.dataSource = self
        
        consejoService.getAdvices(){
            (data) -> () in
            self.consejos = data
            self.tb_consejo.reloadData()
        }
    }
    
    func onShareImage(_ image_url: String,title: String){
        let textToShare = title
        print(textToShare)
        if let myWebsite = URL(string: image_url) {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Consejos"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(ConsejoVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consejos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ConsejoCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ConsejoCellTable
        
        cell.setCell(consejos[indexPath.row],_delegate: self)
        
        return cell
    }

}
