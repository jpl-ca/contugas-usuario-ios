//
//  DistrictE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/28/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
class DistrictE{
    dynamic var code:Int = 0
    dynamic var name:String = ""
    dynamic var parent:String = ""
}