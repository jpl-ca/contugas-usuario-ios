//
//  RegisterIncidentService.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/8/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class RegisterIncidentService{
    let apiHelper = ApiHelper()
    let locationHelper = LocationHelper()
    
    func getIncidentTypes() -> [String:String] {
        var types: [String:String] = [:]
        types["1"] = "Falta de Servicio"
        types["2"] = "Fuga de Gas"
        types["3"] = "Tuberia Rota"
        return types
    }
    
    func initialConfig(){
        locationHelper.configLocationManager()
    }
    
    func sendIncident(_ type:String,address:String,number:String,desc:String,callback: @escaping (Bool)->()) {
        
        let currLocation = locationHelper.getLocation()
        let lat = currLocation.0
        let lng = currLocation.1
        
        let service = "/api/incident"
        let params = ["incident_type_id": type,"comment": desc,
                      "lat": lat,"lng":lng,"address":"\(address) \(number)"]
        print(params)
        apiHelper.makePostRequest(service,params: params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
        
    }
}
