//
//  UserE.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/26/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class UserE: Object {
    dynamic var id:Int = 0
    dynamic var user_id:Int = 0
    dynamic var id_number:String = ""
    dynamic var name:String = ""
    dynamic var email:String = ""
    dynamic var client_number:String = ""
    
    dynamic var address:String = ""
    dynamic var district_text:String = ""
    dynamic var province_id:String = ""
    dynamic var province_text:String = ""
    
    dynamic var gender:String = ""
    dynamic var supply_state:String = ""
    
    func setData(_ data:JSON?){
        user_id = data!["id"].int!
        id_number = data!["id_number"].string!
        client_number = data!["client_number"].string!
        name = data!["name"].string!
        email = data!["email"].string!
        
        address = data!["address"].string!
        district_text = data!["district_text"].string!
        province_id = data!["province_id"].string!
        province_text = data!["province_text"].string!
        
        gender = data!["gender"].string!
        supply_state = data!["supply_state"].string!
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
