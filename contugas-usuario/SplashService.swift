//
//  SplashService.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/25/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class SplashService {
    let apiHelper = ApiHelper()
    let userStore = UserStore()
    
    func checkSessionStatus(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/check"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            // Si dentro de .null hay datos, enviar al error
            if ((data?.null) != nil || data == false ) {
                print(error)
                print("E:****************************")
                self.userStore.removeUserData()
                callback(false)
            }else{
//                print("JSONN: \(data)")
//                print("D:****************************")
                callback(true)
            }
        }
    }
}
