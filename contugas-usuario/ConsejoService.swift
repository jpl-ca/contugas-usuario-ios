//
//  ConsejoService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class ConsejoService {
    let apiHelper = ApiHelper()
    var adviceList:[AdviceE] = []
    
    func getAdvices(_ callback: @escaping ([AdviceE])->()) {
        let service = "/api/tips?page=1"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(self.adviceList)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.parseAdvices(data!)
                callback(self.adviceList)
            }
        }
    }
    
    func parseAdvices(_ data:JSON){
        let couponArray = data["data"].arrayValue
        self.adviceList = [AdviceE]()
        
        for val in couponArray {
            let advice = AdviceE()
            advice.consejo_id = val["id"].int!
            if(val["title"].null == nil){
                advice.title = val["title"].string!
            }
            if(val["image_url"].null == nil){
                advice.image_url = val["image_url"].string!
            }
            
            adviceList.append(advice)
        }
    }
}
