//
//  ConsumptionE.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift

class ConsumptionE : Object{
    dynamic var id:Int = 0
    dynamic var amount_paid:Double = 0
    dynamic var month:Int = 0
    dynamic var month_name:String = ""
    dynamic var amount_saved:Double = 0
    dynamic var year:String = ""
    dynamic var consumption:Double = 0
    override static func primaryKey() -> String? {
        return "id"
    }
}