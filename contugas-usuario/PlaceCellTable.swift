//
//  PlaceCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class PlaceCellTable: UITableViewCell {

    @IBOutlet weak var txt_nombre: UILabel!
    @IBOutlet weak var txt_direccion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ place:PlaceE){
        txt_nombre.text = place.name
        txt_direccion.text = place.address
    }
}
