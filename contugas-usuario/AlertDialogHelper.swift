//
//  AlertDialogHelper.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/28/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import UIKit

class AlertDialogHelper{
    let colorHelper = ColorHelper()
    var uiViewController:UIViewController? = nil
    
    func showAlert(_ controller : UIViewController, title:String, message:String){
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        controller.present(alert, animated: true){}
    }
    
    func showAlert(_ controller : UIViewController,title: String,message:String,callback: @escaping (Bool)->()){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style:
            UIAlertActionStyle.default, handler:{
                action in
                callback(true)
            }
          )
        )
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showLoading(_ msg:String) ->UIAlertController {
        let alertController = UIAlertController(title: nil, message: "\(msg)\n\n", preferredStyle: UIAlertControllerStyle.alert)
        
        let spinnerIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        
        alertController.view.addSubview(spinnerIndicator)
        return alertController
    }
}
