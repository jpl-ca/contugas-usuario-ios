//
//  PaymentAttentionPlacesService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class PaymentAttentionPlacesService {
    let apiHelper = ApiHelper()
    
    func getPaymentAttention(_ callback: @escaping (Bool)->()) {
        let service = "/api/contact-information"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
