//
//  ContuclubTabBar.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class ContuclubTabBar: UITabBarController {
    let colorHelper = ColorHelper()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        self.tabBar.tintColor = colorHelper.colorWhite()
        self.tabBar.barTintColor = colorHelper.colorPrimary()
        self.tabBar.backgroundColor = colorHelper.colorCeleste()
    }
}