//
//  ConsumptionVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/1/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit


/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
/// Deprecated
class ConsumptionVC: UIViewController {
    
    @IBOutlet weak var txtMonthName:UILabel!
    @IBOutlet weak var txtSavedAmount:UILabel!
    @IBOutlet weak var txtPaidAmount:UILabel!
    @IBOutlet weak var txtConsumption:UILabel!
    
    @IBOutlet weak var btnPrevious:UIButton!
    @IBOutlet weak var btnNext:UIButton!
    
    @IBOutlet var containerViewMonth1: UIView!
    @IBOutlet var containerViewMonth2: UIView!
    
    @IBOutlet weak var txtmessage:UILabel!
    @IBOutlet weak var txtM3:UILabel!
    
    @IBOutlet weak var txtMonthNameM1:UILabel!
    @IBOutlet weak var txtSavedAmountM1:UILabel!
    @IBOutlet weak var txtPaidAmountM1:UILabel!
    @IBOutlet weak var txtConsumptionM1:UILabel!
    
    @IBOutlet weak var txtMonthNameM2:UILabel!
    @IBOutlet weak var txtSavedAmountM2:UILabel!
    @IBOutlet weak var txtPaidAmountM2:UILabel!
    @IBOutlet weak var txtConsumptionM2:UILabel!
    
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let consumptionService = ConsumptionService()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniToobar()
        loadData()
    }
    
    @IBAction func nextMonth(_ sender:UIButton){
        self.consumptionService.nextConsumption(){ (consumo,has_prev,has_next) -> () in
            self.btnPrevious.isEnabled = has_prev
            self.btnNext.isEnabled = has_next
            self.showConsumption(consumo)
        }
    }
    
    @IBAction func previousMonth(_ sender:UIButton){
        self.consumptionService.prevConsumption(){ (consumo,has_prev,has_next) -> () in
            self.btnPrevious.isEnabled = has_prev
            self.btnNext.isEnabled = has_next
            self.showConsumption(consumo)
        }
    }
    
    func loadData(){
        consumptionService.loadConsumption(){ (success) -> () in
            if(success){
                self.consumptionService.lastConsumption(){ (consumo,has_prev,has_next) -> () in
                    self.btnPrevious.isEnabled = has_prev
                    self.btnNext.isEnabled = has_next
                    self.showConsumption(consumo)
                }
            }else{
                
            }
        }
    }
    
    func animateSaveAmountValue(_ amount:Double){
        for val in 0...10 {
            UIView.animate(withDuration: 10.5, delay: 12.0, options: UIViewAnimationOptions.curveLinear,animations: {
                self.txtSavedAmount.text = "0.00"
                }, completion: {
                    (value: Bool) in
                    let tmp = Double(val)
                    self.txtSavedAmount.text = self.formatToDecimal(tmp, decimals: 2)
            })
        }
        self.txtSavedAmount.text = self.formatToDecimal(amount, decimals: 2)
    }
    
    func showConsumption(_ consumoLista:[ConsumptionE]) {
        var consumo = consumoLista[0]
        txtMonthName.text = consumo.month_name
        let amount_paid = self.formatToDecimal(consumo.amount_paid, decimals: 2)
        txtPaidAmount.text = amount_paid
        let consumption = self.formatToDecimal(consumo.consumption, decimals: 2)
        txtConsumption.text = consumption
//        animateSaveAmountValue(consumo.consumption)
        let amount_saved = self.formatToDecimal(consumo.amount_saved, decimals: 2)
        txtSavedAmount.text = amount_saved
        let amount_total = self.formatToDecimal(consumo.amount_saved+consumo.amount_paid, decimals: 2)
        txtmessage.text = "*Si este mes usted continuara usando balones de GLP pagaria S/\(amount_total), pero con el gas natural de Contugas solo paga S/ \(amount_paid), teniendo un ahorro de S/\(amount_saved)."
        
        if(consumoLista.count > 1){
            consumo = consumoLista[1]
            txtMonthNameM1.text = consumo.month_name
            let amount_paid = self.formatToDecimal(consumo.amount_paid, decimals: 2)
            txtPaidAmountM1.text = amount_paid
            let consumption = self.formatToDecimal(consumo.consumption, decimals: 2)
            txtConsumptionM1.text = consumption
            let amount_saved = self.formatToDecimal(consumo.amount_saved, decimals: 2)
            txtSavedAmountM1.text = amount_saved
            containerViewMonth1.alpha = 1
        }else{
            containerViewMonth1.alpha = 0
        }
        
        if(consumoLista.count > 2){
            consumo = consumoLista[2]
            txtMonthNameM2.text = consumo.month_name
            let amount_paid = self.formatToDecimal(consumo.amount_paid, decimals: 2)
            txtPaidAmountM2.text = amount_paid
            let consumption = self.formatToDecimal(consumo.consumption, decimals: 2)
            txtConsumptionM2.text = consumption
            let amount_saved = self.formatToDecimal(consumo.amount_saved, decimals: 2)
            txtSavedAmountM2.text = amount_saved
            containerViewMonth2.alpha = 1
        }else{
            containerViewMonth2.alpha = 0
        }
    }
    
    func formatToDecimal(_ value:Double,decimals:Int) -> (String) {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = decimals
        formatter.maximumFractionDigits = decimals
        return formatter.string(from: NSNumber(value))!
    }
    
    func iniToobar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(ConsumptionVC.goBack(_:)), for: .touchUpInside)
        
        let myBtnChart: UIButton = UIButton()
        myBtnChart.setImage(UIImage(named: "chart-50"), for: UIControlState())
        myBtnChart.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnChart.addTarget(self, action: #selector(ConsumptionVC.statisticTapped(_:)), for: .touchUpInside)
        
        let myBtnIncident: UIButton = UIButton()
        myBtnIncident.setImage(UIImage(named: "incident-50"), for: UIControlState())
        myBtnIncident.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnIncident.addTarget(self, action: #selector(ConsumptionVC.incidentTapped(_:)), for: .touchUpInside)
        
        let myBtnContuclub: UIButton = UIButton()
        myBtnContuclub.setImage(UIImage(named: "contuclub-50"), for: UIControlState())
        myBtnContuclub.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnContuclub.addTarget(self, action: #selector(ConsumptionVC.contuclubTapped(_:)), for: .touchUpInside)
        
        let rightGrapBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnChart)
        let rightIncidentBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnIncident)
        let rightContuclubBarButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnContuclub)
        
        self.navigationItem.setRightBarButtonItems([rightGrapBarButtonItem,rightIncidentBarButtonItem,rightContuclubBarButtonItem], animated: true)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
        
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func contuclubTapped (_ sender:UIButton) {
        self.performSegue(withIdentifier: "goto_contuclub", sender: self)
    }
    
    func incidentTapped (_ sender:UIButton) {
        self.performSegue(withIdentifier: "goto_incident", sender: self)
    }
    
    func statisticTapped (_ sender:UIButton) {
        self.performSegue(withIdentifier: "goto_statistics", sender: self)
    }
}
