//
//  ZoneGasVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/11/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit
import GoogleMaps

class ZoneGasVC: UIViewController {
    
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let zoneGasService = ZonaGasService()
    @IBOutlet weak var viewMap: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        setupButtonPlaces()
        setupMap()
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = VAR.TitleVC.zonas_gas_natural
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(ZoneGasVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupMap(){
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.settings.zoomGestures = true
        
        zoneGasService.getZones()
        let area = zoneGasService.showZoneIca()
        showAreas(area)
    }
    
    func showAreas(_ area:ZoneE){
        viewMap.clear()
        let path = GMSMutablePath()
        for area_zone in area.zones{
            let rect = GMSMutablePath()
            for area_point in area_zone{
                rect.add(CLLocationCoordinate2DMake(area_point.lat, area_point.lng))
                path.add(CLLocationCoordinate2DMake(area_point.lat, area_point.lng))
            }
            let polygon = GMSPolygon(path: rect)
            polygon.fillColor = colorHelper.colorOrange(0.5);
            polygon.strokeColor = colorHelper.colorOrange()
            polygon.strokeWidth = 2
            polygon.map = viewMap
        }
        
        let bounds = GMSCoordinateBounds(path: path)
        
        viewMap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 35.0))
    }
    
    
    func setupButtonPlaces() {
        addMenuOptionZone(1,name:"Marcona",image:"nasca",color:colorHelper.colorNasca(),buttonAction:#selector(ZoneGasVC.BuscarMarcona(_:)))
        addMenuOptionZone(2,name:"Nasca",image:"nasca",color:colorHelper.colorNasca(),buttonAction:#selector(ZoneGasVC.BuscarNasca(_:)))
        addMenuOptionZone(3,name:"Ica",image:"ica",color:colorHelper.colorIca(),buttonAction:#selector(ZoneGasVC.BuscarIca(_:)))
        addMenuOptionZone(4,name:"Pisco",image:"pisco",color:colorHelper.colorPisco(),buttonAction:#selector(ZoneGasVC.BuscarPisco(_:)))
        addMenuOptionZone(5,name:"Chincha",image:"chincha",color:colorHelper.colorChincha(),buttonAction:#selector(ZoneGasVC.BuscarChincha(_:)))
    }
    
    func BuscarIca(_ sender:UIButton) {
        let area = zoneGasService.showZoneIca()
        showAreas(area)
    }
    func BuscarPisco(_ sender:UIButton) {
        let area = zoneGasService.showZonePisco()
        showAreas(area)
    }
    func BuscarNasca(_ sender:UIButton) {
        let area = zoneGasService.showZoneNasca()
        showAreas(area)
    }
    func BuscarChincha(_ sender:UIButton) {
        let area = zoneGasService.showZoneChincha()
        showAreas(area)
    }
    func BuscarMarcona(_ sender:UIButton) {
        let area = zoneGasService.showZoneMarcona()
        showAreas(area)
    }
    
    func addMenuOptionZone(_ pos:Int, name:String, image:String, color:UIColor, buttonAction:Selector){
        let h = 45
        let w = 45
        let space_button_map = 150
        let paddingRight = CGFloat(55)
        let paddingBottom = CGFloat(space_button_map + ( ( h + 10 ) * pos ) )
        
        let X_Pos = self.view.frame.size.width - paddingRight
        let Y_Pos = self.view.frame.size.height - paddingBottom
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: X_Pos, y: Y_Pos, width: CGFloat(w), height: CGFloat(h))
        button.layer.cornerRadius = 0.5 * button.bounds.size.width
        button.backgroundColor = color
        button.setImage(UIImage(named:image), for: UIControlState())
        button.imageEdgeInsets = UIEdgeInsetsMake(35,35,35,35)
        button.addTarget(self, action: buttonAction, for: .touchUpInside)
        view.addSubview(button)
        addLabel(name, x: X_Pos - CGFloat( w + 27), y: Y_Pos + 12.5)
    }
    
    func addLabel(_ name:String,x:CGFloat,y:CGFloat){
        let label = UILabel(frame: CGRect(x: x, y: y, width: 70, height: 21))
        label.textAlignment = NSTextAlignment.center
        label.text = name
        
        label.backgroundColor = colorHelper.colorGrayDark()
        label.textColor = colorHelper.colorWhite()
        
        label.font = UIFont(name: label.font.fontName, size: 13)
        
        view.addSubview(label)
    }
    
}
