//
//  UbivanosService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/28/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class UbicanosService{
    
    let locationHelper = LocationHelper()
    let apiHelper = ApiHelper()
    
    var paymentAttentionPlaceE = [PaymentAttentionPlaceE]()
    
    init(){
        locationHelper.configLocationManager()
    }
    
    func myLocation() -> (Double,Double){
        let currLocation = locationHelper.getLocation()
        let lat = currLocation.0
        let lng = currLocation.1
        return (Double(lat)!,Double(lng)!)
    }
    
    func getAllPlaces(_ callback: @escaping ([PaymentAttentionPlaceE])->()) {
        let service = "/api/contact-information"
        print("Request To -> \(service)")
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos
                print(error)
                print("E:****************************")
                callback(self.paymentAttentionPlaceE)
            }else{
//                print("JSONN: \(data)")
                print("D:****************************")
                self.parsePlace(data)
                callback(self.paymentAttentionPlaceE)
            }
        }
    }
    
    func parsePlace(_ data:JSON?){
        let attention_places = data!["attention_places"].arrayValue
        let payment_places = data!["payment_places"].arrayValue
        let caja_ica_places = data!["caja_ica_places"].arrayValue
        
        var placeList = [PlaceE]()
        
        for val in attention_places {
            let place = PlaceE()
            place.name = val["name"].string!
            place.address = val["address"].string!
            place.attention_hour = val["attention_hour"].string!
            place.phone = val["phone"].string!
            place.lat = val["lat"].double!
            place.lng = val["lng"].double!
            placeList.append(place)
        }
        var paymentAttentionPlace = PaymentAttentionPlaceE()
        paymentAttentionPlace.name = "Centros de Atención al Cliente"
        paymentAttentionPlace.places = placeList
        paymentAttentionPlace.place_type = PLACE_TYPE.markerCenterOfAttention
        paymentAttentionPlaceE.append(paymentAttentionPlace)
        
        
        placeList = [PlaceE]()
        for val in payment_places {
            let place = PlaceE()
            place.name = val["name"].string!
            place.address = val["address"].string!
            place.attention_hour = val["attention_hour"].string!
            place.phone = val["phone"].string!
            place.lat = val["lat"].double!
            place.lng = val["lng"].double!
            placeList.append(place)
        }
        paymentAttentionPlace = PaymentAttentionPlaceE()
        paymentAttentionPlace.name = "Puntos de Atención al Cliente"
        paymentAttentionPlace.places = placeList
        paymentAttentionPlace.place_type = PLACE_TYPE.markerPointOfAttention
        paymentAttentionPlaceE.append(paymentAttentionPlace)
        
        
        for val in caja_ica_places {
            let region_name = val["region_name"].string!
            let region_places = val["region_places"].arrayValue
            
            placeList = [PlaceE]()
            for val in region_places {
                let place = PlaceE()
                place.name = val["name"].string!
                place.address = val["address"].string!
                place.attention_hour = val["attention_hour"].string!
                place.phone = val["phone"].string!
                place.lat = val["lat"].double!
                place.lng = val["lng"].double!
                placeList.append(place)
            }
            paymentAttentionPlace = PaymentAttentionPlaceE()
            paymentAttentionPlace.name = "Caja Municipal Ica - \(region_name)"
            paymentAttentionPlace.places = placeList
            paymentAttentionPlace.place_type = PLACE_TYPE.markerCajaIca
            paymentAttentionPlaceE.append(paymentAttentionPlace)
            
        }
    }
}
