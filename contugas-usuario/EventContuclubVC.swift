//
//  EventContuclubVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/13/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class EventContuclubVC: UIViewController, UITableViewDataSource, UITableViewDelegate, EventDetailDelegate {
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let contuclubService = EventContuclubService()
    
    @IBOutlet weak var tb_event: UITableView!
    
    var eventos : [EventE] = [EventE]()
    var idxEventSelected = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tb_event.delegate = self
        tb_event.dataSource = self
        
        contuclubService.getEvents(){
            (r) -> () in
            self.eventos = r
            self.tb_event.reloadData()
        }
        
        setupToolbar()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        idxEventSelected = indexPath.row
        self.performSegue(withIdentifier: "goto_evento_detail", sender: self)
    }
    
    func attendToEvent(_ attend: Bool) {
        if(eventos[idxEventSelected].attending != attend){
            self.eventos[idxEventSelected].attending = attend
            let indexPath = IndexPath(row: idxEventSelected, section: 0)
            self.tb_event.reloadRows(at: [indexPath], with: UITableViewRowAnimation.top)
            if(attend){
                contuclubService.imGoingToAttend(self.eventos[idxEventSelected].event_id){
                    (b) -> () in
                    
                }
            }else{
                contuclubService.imNotGoingToAttend(eventos[idxEventSelected].event_id){
                    (b) -> () in
                    
                }
            }
        }
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventos.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController2 = segue.destination as? EventoDetailVC {
            viewController2.evento = eventos[idxEventSelected]
            viewController2.delegate = self
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EventCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! EventCellTable
        
        cell.setCell(eventos[indexPath.row])
        
        return cell
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Eventos"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(EventContuclubVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
}
