//
//  QuotationRequestVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/7/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class QuotationRequestVC: UIViewController,UITextViewDelegate {
    let quotationRequestService = QuotationRequestService()
    @IBOutlet weak var txtNombres:UITextField!
    @IBOutlet weak var txtApellidos:UITextField!
    @IBOutlet weak var txtUbicacion:UITextField!
    @IBOutlet weak var txtProvince:UITextField!
    @IBOutlet weak var txtDistrict:UITextField!
    @IBOutlet weak var txtTelefono:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtComentario:UITextView!
    @IBOutlet weak var btn_consultar_mapa: UIButton!
    @IBOutlet weak var btn_enviar: UIButton!
    
    var provinces:[String:ProvinceE] = [:]
    var province_selected:String = ""
    var province_selected_text = ""
    
    var districts:[String:DistrictE] = [:]
    var district_selected:String = ""
    var district_selected_text = ""
    
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let alertDialogHelper = AlertDialogHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        self.txtComentario.delegate = self
        styleHelper.setBorder(txtNombres)
        styleHelper.setBorder(txtApellidos)
        styleHelper.setBorder(txtUbicacion)
        styleHelper.setBorder(txtProvince)
        styleHelper.setBorder(txtDistrict)
        styleHelper.setBorder(txtTelefono)
        styleHelper.setBorder(txtEmail)
        
        styleHelper.setBorder(txtComentario)
        btn_enviar.backgroundColor = colorHelper.colorGreenButton()
        
        quotationRequestService.getUbigeo(){
            (res,provinces,districts) -> () in
            if res{
                self.provinces = provinces
                self.districts = districts
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func sendRequestTapped(_ sender:UIButton){
        let nombres:NSString = txtNombres.text! as NSString
        let apellidos:NSString = txtApellidos.text! as NSString
        let ubicacion:NSString = txtUbicacion.text! as NSString
        
        let telefono:NSString = txtTelefono.text! as NSString
        let email:NSString = txtEmail.text! as NSString
        let comentario:NSString = txtComentario.text! as NSString
        
        if nombres.length==0||apellidos.length==0||ubicacion.length==0||email.length==0{
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Debe completar los datos del formulario.")
            return
        }
        
        if comentario.length < 15{
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Ingrese un comentario de 15 caracteres como mínimo.")
            return
        }
        
        if province_selected == "" {
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Seleccione una provincia.")
            return
        }
        
        if provinces[province_selected]==nil {
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Seleccione una provincia válida.")
            return
        }
        
        if district_selected == "" {
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Seleccione un distrito.")
            return
        }
        
        if districts[district_selected]==nil {
            alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Seleccione un distrito válido.")
            return
        }
        
        quotationRequestService.sendRequest(nombres as String,apellidos: apellidos as String,ubicacion: ubicacion as String,provincia: province_selected,distrito: district_selected,telefono: telefono as String,email: email as String,comentario:comentario as String){(result) -> () in
            if result {
                self.showAlertRequest("Sus datos fueron enviados correctamente.")
            }else{
                self.alertDialogHelper.showAlert(self,title: "Contugas - Solicitud de Presupuesto",message: "Hay error en sus datos, revisa antes de enviar.")
            }
        }
    }
    
    func showAlertProvinces(){
        let alert = UIAlertController(title: "Contugas", message: "Seleccione una provincia", preferredStyle: UIAlertControllerStyle.alert)
        
        for (key,value) in provinces {
            alert.addAction(UIAlertAction(title: "\(value.name)", style: UIAlertActionStyle.default, handler: {
                action in
                self.province_selected = "\(key)"
                self.province_selected_text = "\(value.name)"
                self.txtProvince.text = self.province_selected_text
                
                self.district_selected = ""
                self.district_selected_text = ""
                self.txtDistrict.text = ""
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertDisctricts(){
        let alert = UIAlertController(title: "Contugas", message: "Seleccione un Distrito", preferredStyle: UIAlertControllerStyle.alert)
        
        for (key,value) in districts {
            if(self.province_selected == value.parent){
                alert.addAction(UIAlertAction(title: "\(value.name)", style: UIAlertActionStyle.default, handler: {
                    action in
                    self.district_selected = "\(key)"
                    self.district_selected_text = "\(value.name)"
                    self.txtDistrict.text = self.district_selected_text
                }))
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertRequest(_ message:String){
        let alert = UIAlertController(title: "Contugas", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{action in self.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    @IBAction func selectProvinceTapped(_ sender:AnyObject){
        self.showAlertProvinces()
    }
    
    @IBAction func selectDistrictTapped(_ sender:AnyObject){
        if(!self.province_selected.isEmpty){
            self.showAlertDisctricts()
        }
    }
    
    @IBAction func cancelTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showMapTapped(_ sender:UIButton){
        print("asdl ds ol a k ase sese ...")
    }
    
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        navigationItem.title = "Solicitar Presupuesto"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(QuotationRequestVC.goBack(_:)), for: .touchUpInside)
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
    
    @IBAction func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
