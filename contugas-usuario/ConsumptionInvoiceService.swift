//
//  ConsumptionInvoiceService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class ConsumptionInvoiceService {
    let apiHelper = ApiHelper()
    let consumptionStore = ConsumptionStore()
    let downloadService = DownloadService()
    let jmStore = JMStore()
    var lastSelected:Int = 0
    var consumptionList:[ConsumptionInvoiceE] = [ConsumptionInvoiceE]()
    
    func getConsumptionInvoice(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/gas-consumption"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                
                let mess:String = data!["saved_money"].string!
                
                self.jmStore.saveData(VAR.Message.invoice_saved_money, value: mess)
                
                let consumptions = data!["data"].arrayValue
                self.consumptionList = [ConsumptionInvoiceE]()
                for obj in consumptions {
                    var period = obj["period"].string!
                    
                    let fechas = period.characters.split{$0 == "-"}.map(String.init)
                    
                    let fechaIni = fechas[0].characters.split{$0 == "/"}.map(String.init)
                    let fechaFin = fechas[1].characters.split{$0 == "/"}.map(String.init)
                    
                    var index1 = fechaIni[2].characters.index(fechaIni[2].endIndex, offsetBy: -3)
                    var subs = fechaIni[2].substring(from: index1)
                    period = "\(fechaIni[0])/\(fechaIni[1])/\(subs)"
                    
                    index1 = fechaFin[2].characters.index(fechaFin[2].endIndex, offsetBy: -2)
                    subs = fechaFin[2].substring(from: index1)
                    period = "\(period) - \(fechaFin[0])/\(fechaFin[1])/\(subs)"
                    
                    let rate_category = obj["rate_category"].string!
                    let volume_invoice = obj["volume_invoice"].string!
                    let total_invoice = obj["total_invoice"].string!
                    let last_debt  = obj["last_debt"].string!
                    let current_debt  = obj["current_debt"].string!
                    let month_name  = obj["month_name"].string!
                    let expiration_date  = obj["expiration_date"].string!
                    let year  = obj["year"].string!
                    let month  = obj["month"].string!
                    
                    let historialPago = ConsumptionInvoiceE()
                    historialPago.setData(period, rate_category:rate_category, volume_invoice: volume_invoice, total_invoice:total_invoice, last_debt: last_debt, current_debt:current_debt, month: month, month_name:month_name, year: year, expiration_date:expiration_date)
                    self.consumptionList.append(historialPago)
                }
                self.consumptionList = self.consumptionList.reversed()
                self.consumptionStore.storeConsumptions(self.consumptionList)
                callback(consumptions.count>0)
            }
        }
    }
    
    func downloadInvoicePdf(_ callback: @escaping (Bool)->()){
        let service = "/api/user/auth/me/last-billing"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.parseToUrlPdf(data)
                callback(true)
            }
        }
    }
    
    func parseToUrlPdf(_ data:JSON?){
        let pdf_url:String = data!["data"]["url"].string!;
        downloadService.downloadPdf(pdf_url, callback: pdf_url)
    }
    
    func getConsumptionList(_ callback: ([ConsumptionInvoiceE])->()) {
        consumptionStore.getConsumption(){ (consumptionList) -> () in
            callback(consumptionList)
        }
    }
    
    func getMessageInvoiceSavedMoney() -> String{
        let m = jmStore.getData(VAR.Message.invoice_saved_money)
        return m as! String
    }
    
    
    
    func lastConsumption(_ callback: (ConsumptionInvoiceE, Bool,Bool)->()) {
        lastSelected = consumptionList.count - 1
        getConsumption(lastSelected){ (consumption, has_prev,has_next) -> () in
            callback(consumption, has_prev,has_next)
        }
    }
    func prevConsumption(_ callback: (ConsumptionInvoiceE, Bool,Bool)->()) {
        if(lastSelected > 0){
            lastSelected = lastSelected - 1
            getConsumption(lastSelected){ (consumption, has_prev,has_next) in
                callback(consumption, has_prev,has_next)
            }
        }
    }
    func nextConsumption(_ callback: (ConsumptionInvoiceE, Bool,Bool)->()) {
        if(lastSelected < consumptionList.count-1){
            lastSelected = lastSelected + 1
            getConsumption(lastSelected){ (consumption, has_prev,has_next) in
                callback(consumption, has_prev,has_next)
            }
        }
    }
    fileprivate func getConsumption(_ indexListMonth:Int,callback: (ConsumptionInvoiceE, Bool,Bool)->()) {
        let has_next = lastSelected < consumptionList.count-1
        let has_prev = lastSelected > 0
        callback(consumptionList[indexListMonth],has_prev,has_next)
    }
    
    
    
    func downloadLastInvoice(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/last-billing"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                
                callback(true)
            }
        }
    }
    
    func getPaymentHistory(_ callback: @escaping ([HistorialPagoE])->()) {
        let service = "/api/user/auth/me/payment-history"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                let payments = data!["data"].arrayValue
                var historialPagoList:[HistorialPagoE] = [HistorialPagoE]()
                for obj in payments {
                    let fecha = obj["date"].string!
                    let lugar = obj["place"].string!
                    let importe = obj["amount"].string!
                    let historialPago = HistorialPagoE(fecha: fecha, lugar:lugar, importe: importe)
                    historialPagoList.append(historialPago)
                }
                callback(historialPagoList)
            }
        }
    }
    
    func getFinancing(_ callback: @escaping ([FinancingE])->()) {
        let service = "/api/user/auth/me/financing"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                let payments = data!["data"].arrayValue
                var financiamientoList:[FinancingE] = [FinancingE]()
                for obj in payments {
                    let tipo = obj["type"].string!
                    let total_financiado = obj["total"].string!
                    let cuotas_facturadas = obj["invoiced_quote"].string!
                    
                    let cuotas_pactadas = obj["agreed_quote"].string!
                    let cuotas_pendientes = obj["pending_quote"].string!
                    let valor_cuota = obj["quote_value"].string!
                    
                    let historialPago = FinancingE(tipo: tipo,total_financiado: total_financiado,cuotas_facturadas: cuotas_facturadas,cuotas_pactadas: cuotas_pactadas,cuotas_pendientes: cuotas_pendientes,valor_cuota: valor_cuota)
                    financiamientoList.append(historialPago)
                }
                callback(financiamientoList)
            }
        }
    }
}
