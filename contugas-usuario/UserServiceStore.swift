//
//  UserServiceStore.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/26/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserServiceStore {
    
    
    //    @deprecated
    func verificarSiGuardoSesion(){
        let prefs:UserDefaults = UserDefaults.standard
        let isLoggedIn:Int = prefs.integer(forKey: "ISLOGGEDIN") as Int
        if (isLoggedIn != 1) {
            
        } else {
        }
    }
    //    @deprecated
    func guardarSesion(_ data:JSON?){
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(data!["name"].string, forKey: "USERNAME")
        prefs.set(data!["id_number"].string, forKey: "ID_NUMBER")
        prefs.set(data!["email"].string, forKey: "EMAIL")
        prefs.set(1, forKey: "ISLOGGEDIN")
        prefs.synchronize()
    }
    
    //    @deprecated
    func getUserProfileInfo(){
        let prefs:UserDefaults = UserDefaults.standard
        let usernameLabel = prefs.value(forKey: "USERNAME") as? String
        let idnumberLabel = prefs.value(forKey: "ID_NUMBER") as? String
        let emailLabel = prefs.value(forKey: "EMAIL") as? String
        print(usernameLabel)
        print(idnumberLabel)
        print(emailLabel)
    }
}
