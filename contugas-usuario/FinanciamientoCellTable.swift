//
//  FinanciamientoCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class FinanciamientoCellTable: UITableViewCell {

    @IBOutlet weak var txt_titulo: UILabel!
    @IBOutlet weak var txt_tot_financiado: UILabel!
    @IBOutlet weak var txt_cuota_facturada: UILabel!
    @IBOutlet weak var txt_cuota_pactada: UILabel!
    @IBOutlet weak var txt_cuota_pendiente: UILabel!
    @IBOutlet weak var txt_valor_cuota: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ financing:FinancingE){
        txt_titulo.text = financing.tipo
        txt_tot_financiado.text = financing.total_financiado
        txt_cuota_facturada.text = financing.cuotas_facturadas
        txt_cuota_pactada.text = financing.cuotas_pactadas
        txt_cuota_pendiente.text = financing.cuotas_pendientes
        txt_valor_cuota.text = financing.valor_cuota
    }
}
