//
//  RegisterAccountVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/18/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class RegisterAccountVC: UIViewController {
    @IBOutlet weak var txtNumCli:UITextField!
    @IBOutlet weak var txtIdentityDoc:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblPassword:UILabel!
    @IBOutlet weak var btnRegister:UIButton!
    @IBOutlet weak var btn_cancelar: UIButton!
    
    var validated = false
    
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    let registerAccountService = RegisterAccountService()
    let alertDialog = AlertDialogHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleHelper.setBorder(txtNumCli)
        styleHelper.setBorder(txtIdentityDoc)
        styleHelper.setBorder(txtEmail)
        styleHelper.setBorder(txtPassword)
        txtEmail.isEnabled = false
        txtPassword.isEnabled = false
        btnRegister.backgroundColor = colorHelper.colorGreenButton()
        btn_cancelar.tintColor = colorHelper.colorGreenButton()
    }
    
    @IBAction func goBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goRegister(_ sender:UIButton){
        let numCli = txtNumCli.text!
        let identityDoc = txtIdentityDoc.text!
        let email = txtEmail.text!
        let pass = txtPassword.text!
        
        if(!validated){
            registerAccountService.ValidateUser(numCli, identification_document: identityDoc){
                (b) -> () in
                if(b){
                    self.validated = true
                    self.txtEmail.isEnabled = true
                    self.txtPassword.isEnabled = true
                    self.txtNumCli.isEnabled = false
                    self.txtIdentityDoc.isEnabled = false
                    self.alertDialog.showAlert(self, title: "Contugas - Registro de Cuenta", message: "Sus datos fueron verificados correctamente, complete su información"){
                        (b) -> () in
                        self.btnRegister.setTitle(VAR.Button.Registrar, for: UIControlState())
                    }
                }else{
                    self.alertDialog.showAlert(self, title: "Contugas - Registro de Cuenta", message: "Sus datos no son válidos, corregir para continuar")
                }
            }
        }else{
            registerAccountService.RegisterUser(numCli, identification_document: identityDoc,email: email, password: pass){
                (b) -> () in
                if(b){
                    self.txtEmail.isEnabled = false
                    self.txtPassword.isEnabled = false
                    self.txtNumCli.isEnabled = false
                    self.txtIdentityDoc.isEnabled = false
                    self.alertDialog.showAlert(self, title: "Contugas - Registro de Cuenta", message: "Sus registro fue correcto, puede ingresar en cualquier momento"){
                        (b) -> () in
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }else{
                    self.alertDialog.showAlert(self, title: "Contugas - Registro de Cuenta", message: "Sus datos ingresados no son válidos, intente de nuevo")
                }
            }
        }
    }
}
