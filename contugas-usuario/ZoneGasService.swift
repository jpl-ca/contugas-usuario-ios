//
//  ZoneGasService.swift
//  contugas-usuario
//
//  Created by JM Tech on 5/4/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class ZonaGasService{
    let zoneGas = ZoneGasE()
    func getZones(){
        let asset = NSDataAsset(name: "gas_zone", bundle: Bundle.main)
        let json = try? JSONSerialization.jsonObject(with: asset!.data, options: JSONSerialization.ReadingOptions.allowFragments)
        let data = JSON(json!)
        parseZone(data)
    }
    
    func getZonePoints(_ name:String,data:JSON?) -> (lat:Double, lng:Double, zoneArea:[[ZonePointE]]) {
        let icaPoints = data![name]["zone"].arrayValue
        var zoneArea = [[ZonePointE]]()
        for zones in icaPoints {
            var zonePoints = [ZonePointE]()
            for val in zones.arrayValue {
                let zp = ZonePointE()
                zp.lat = val["lat"].double!  // double
                zp.lng = val["lng"].double!  // double
                zonePoints.append(zp)
            }
            zoneArea.append(zonePoints)
        }
        return (data![name]["lat"].double!,data![name]["lng"].double!,zoneArea)
    }
    
    func parseZone(_ data:JSON?){
        /**
            ICA
         */
        let icaZone = getZonePoints("Ica", data: data)
        let ica = ZoneE()
        ica.lat = icaZone.lat
        ica.lng = icaZone.lng
        ica.zones = icaZone.zoneArea
        
        zoneGas.Ica = ica
        
        
        
        /**
            PISCO
         */
        let piscoZone = getZonePoints("Pisco", data: data)
        let pisco = ZoneE()
        pisco.lat = piscoZone.lat
        pisco.lng = piscoZone.lng
        pisco.zones = piscoZone.zoneArea
        
        zoneGas.Pisco = pisco
        
        
        
        /**
            NASCA
         */
        let nascaZone = getZonePoints("Nazca", data: data)
        let nasca = ZoneE()
        nasca.lat = nascaZone.lat
        nasca.lng = nascaZone.lng
        nasca.zones = nascaZone.zoneArea
        
        zoneGas.Nasca = nasca

        
        
        /**
         CHINCHA
         */
        let chinchaZone = getZonePoints("Chincha", data: data)
        let chincha = ZoneE()
        chincha.lat = chinchaZone.lat
        chincha.lng = chinchaZone.lng
        chincha.zones = chinchaZone.zoneArea
        
        zoneGas.Chincha = chincha
        
        
        
        /**
            MARCONA
         */
        let marconaZone = getZonePoints("Marcona", data: data)
        let marcona = ZoneE()
        marcona.lat = marconaZone.lat
        marcona.lng = marconaZone.lng
        marcona.zones = marconaZone.zoneArea
        
        zoneGas.Marcona = marcona
    }
    
 
    func showZoneIca() -> (ZoneE) {
        return zoneGas.Ica
    }
    
    func showZoneChincha() -> (ZoneE) {
        return zoneGas.Chincha
    }
    
    func showZoneMarcona() -> (ZoneE) {
        return zoneGas.Marcona
    }
    
    func showZoneNasca() -> (ZoneE) {
        return zoneGas.Nasca
    }
    
    func showZonePisco() -> (ZoneE) {
        return zoneGas.Pisco
    }
    
}
