//
//  DownloadService.swift
//  contugas-usuario
//
//  Created by JM Tech on 3/9/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation

import SwiftyJSON
import RealmSwift

class DownloadService {
    let apiHelper = ApiHelper()
    
    func downloadImage(_ img_url:String,callback: @escaping (UIImage)->()) {
        
        if UIApplication.shared.canOpenURL(URL(string: img_url)!){
            apiHelper.downloadImage(img_url){ (image) -> () in
                callback(image)
            }
        }
    }
    
    func downloadPdf(_ pdf_url:String,callback:String){
        UIApplication.shared.openURL(URL(string: pdf_url)!)
    }
    
    func doanloadFile(_ pdf_url:String){
        
    }
}
