//
//  EventService.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/19/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class EventInvoiceService {
    let apiHelper = ApiHelper()
    
    func getEvents(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events?type=event"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func getRaffles(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events?type=raffle"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func imGoingToAttend(_ event_id:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events/\(event_id)/participations"
        let params = ["state": 1]
        apiHelper.makePostRequest(service,params:params){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
    
    func imNotGoingToAttend(_ event_id:String,callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/events/\(event_id)/participations"
        apiHelper.makeDeleteRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                callback(true)
            }
        }
    }
}
