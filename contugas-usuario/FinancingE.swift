//
//  FinancingE.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/15/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import Foundation
import RealmSwift

class FinancingE{
    dynamic var tipo:String = ""
    dynamic var total_financiado:String = ""
    dynamic var cuotas_facturadas:String = ""
    dynamic var cuotas_pactadas:String = ""
    dynamic var cuotas_pendientes:String = ""
    dynamic var valor_cuota:String = ""
    
    init(tipo:String,total_financiado:String,cuotas_facturadas:String,cuotas_pactadas:String,cuotas_pendientes:String,valor_cuota:String){
        self.tipo = tipo
        self.total_financiado = total_financiado
        self.cuotas_facturadas = cuotas_facturadas
        self.cuotas_pactadas = cuotas_pactadas
        self.cuotas_pendientes = cuotas_pendientes
        self.valor_cuota = valor_cuota
    }
}