//
//  SorteoContuclubVC.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/13/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class SorteoContuclubVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    let colorHelper = ColorHelper()
    let styleHelper = StyleHelper()
    @IBOutlet var tb_sorteo: UITableView!

    let raffleService = RaffleContuclubService()
    var currentRaffle = RaffleE()
    var raffles : [RaffleE] = [RaffleE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToolbar()
        tb_sorteo.delegate = self
        tb_sorteo.dataSource = self
        
        raffleService.getRaffles(){
            (r) -> () in
            self.raffles = r
            self.tb_sorteo.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return raffles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SorteoCellTable = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SorteoCellTable
        
        cell.setCell(raffles[indexPath.row])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController2 = segue.destination as? SorteoDetailVC {
            viewController2.sorteo = currentRaffle
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentRaffle = raffles[indexPath.row]
        self.performSegue(withIdentifier: "goto_sorteo_detail", sender: self)
    }
    
    func goBack(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupToolbar() {
        styleHelper.setNavigationBarStyle(self.navigationController!)
        self.navigationItem.title = "Sorteos"
        
        let myBtnBack: UIButton = UIButton()
        myBtnBack.setImage(UIImage(named: "back-arrow"), for: UIControlState())
        myBtnBack.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        myBtnBack.addTarget(self, action: #selector(SorteoContuclubVC.goBack(_:)), for: .touchUpInside)
        
        
        let leftBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: myBtnBack)
        self.navigationItem.setLeftBarButtonItems([leftBackButtonItem], animated: true)
    }
}
