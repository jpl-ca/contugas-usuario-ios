//
//  ConsumptionService.swift
//  contugas-usuario
//
//  Created by JM Tech on 2/29/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//


import Foundation
import SwiftyJSON
import RealmSwift


//TODO Deprecateddddd
class ConsumptionService {
    let apiHelper = ApiHelper()
    let consumptionStore = ConsumptionStore()
    var consumptionMap: [Int:ConsumptionE] = [:]
    var consumptionlist = Array<ConsumptionE>()
    var lastSelected:Int = 0
    
    func loadConsumption(_ callback: @escaping (Bool)->()) {
        let service = "/api/user/auth/me/gas-consumption"
        apiHelper.makeGetRequest(service){ (data, error) -> () in
            if (data?.null) != nil {// Si dentro de .null hay datos, enviar al error
                print(error)
                print("E:****************************")
                callback(false)
            }else{
                print("JSONN: \(data)")
                print("D:****************************")
                self.processDataConsuption(data)
                callback(true)
            }
        }
    }
    
    func processDataConsuption(_ dataA:JSON?) {
        let gasConsumption = dataA!["gas-consumption"].arrayValue
        var idx:Int = -1
        for data in gasConsumption {
            idx = idx+1
            if(idx==0){
                continue
            }
            let cons = ConsumptionE()
            cons.id = data["id"].int!
            cons.year = data["year"].string!
            cons.amount_paid = Double(data["amount_paid"].string!)!
            cons.month = Int(data["month"].string!)!
            cons.month_name = data["month_name"].string!
            cons.amount_saved = Double(data["amount_saved"].string!)!
            cons.consumption = Double(data["consumption"].string!)!
            consumptionMap[cons.month] = cons
            consumptionlist.append(cons)
        }
        consumptionlist = consumptionlist.reversed()
//        consumptionStore.storeConsumptions(consumptionlist)
    }
    
    func getConsumptionList(_ callback: ([ConsumptionE])->()) {
        consumptionStore.getConsumption(){ (consumptionList) -> () in
//            callback(consumptionList)
        }
    }
    
    func lastConsumption(_ callback: ([ConsumptionE], Bool,Bool)->()) {
        lastSelected = consumptionlist.count - 1
        getConsumption(lastSelected){ (consumption, has_prev,has_next) -> () in
            callback(consumption, has_prev,has_next)
        }
    }

    func prevConsumption(_ callback: ([ConsumptionE], Bool,Bool)->()) {
        if(lastSelected > 0){
            lastSelected = lastSelected - 1
            getConsumption(lastSelected){ (consumption, has_prev,has_next) in
                callback(consumption, has_prev,has_next)
            }
        }
    }
    func nextConsumption(_ callback: ([ConsumptionE], Bool,Bool)->()) {
        if(lastSelected < consumptionlist.count-1){
            lastSelected = lastSelected + 1
            getConsumption(lastSelected){ (consumption, has_prev,has_next) in
                callback(consumption, has_prev,has_next)
            }
        }
    }

    fileprivate func getConsumption(_ indexListMonth:Int,callback: ([ConsumptionE], Bool,Bool)->()) {
        let has_next = lastSelected < consumptionlist.count-1
        let has_prev = lastSelected > 0
        var consumption = Array<ConsumptionE>()
        consumption.append(consumptionlist[indexListMonth])
        if(lastSelected >= 1) {
            consumption.append(consumptionlist[indexListMonth-1])
        }
        if(lastSelected >= 2) {
            consumption.append(consumptionlist[indexListMonth-2])
        }
        callback(consumption,has_prev,has_next)
    }
    
    fileprivate func getConsumptionMonth(_ month:Int,callback: (ConsumptionE, Bool)->()) {
        var consumption = ConsumptionE()
        var _error = false
        if(consumptionMap[month]==nil){
            _error = true
        }else{
            consumption = consumptionMap[month]!
            _error = false
        }
        callback(consumption,_error)
    }
}
