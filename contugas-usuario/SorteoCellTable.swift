//
//  SorteoCellTable.swift
//  contugas-usuario
//
//  Created by JM Tech on 4/14/16.
//  Copyright © 2016 JM Tech. All rights reserved.
//

import UIKit

class SorteoCellTable: UITableViewCell {
    @IBOutlet weak var iv_sorteo: UIImageView!
    @IBOutlet weak var txt_title: UILabel!
    
    let downloadService = DownloadService()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(_ raffle:RaffleE){
        txt_title.text = raffle.title
        downloadService.downloadImage(raffle.image_url){
            (image) -> () in
            self.iv_sorteo.image = image
        }
        
    }
}
